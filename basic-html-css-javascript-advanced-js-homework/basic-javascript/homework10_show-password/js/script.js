"use strict";
const passwordForm = document.getElementById('reg-form');
const password = document.getElementById('password');
const FPassEnter = password.getElementsByTagName('input')[0];
const oIconPassEnter = password.getElementsByTagName('i')[0];
const rPassword = document.getElementById('rPassword');
const fldPassConfirm = rPassword.getElementsByTagName('input')[0];
const oIconPassConfirm = rPassword.getElementsByTagName('i')[0];

passwordForm.addEventListener('submit',function (event) {
    fnCheckPassEnterConfirm(event);
});
let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    error.style.color = 'red';
    error.style.paddingBottom = '15px';
    error.innerHTML = text;
    return error;
};
let error;
oIconPassEnter.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            FPassEnter.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            FPassEnter.setAttribute('type','password');
            break;
    }
});
oIconPassConfirm.addEventListener('click', function(){
    switch (this.className) {
        case 'fas fa-eye icon-password':
            this.className = 'fas fa-eye-slash icon-password';
            fldPassConfirm.setAttribute('type','text');
            break;
        case 'fas fa-eye-slash icon-password':
            this.className = 'fas fa-eye icon-password';
            fldPassConfirm.setAttribute('type','password');
            break;
    }
});
function fnCheckPassEnterConfirm(event){
    if(FPassEnter.value===''){
        error = generateError('Вы забыли ввести пароль');
        password.append(error);
        event.preventDefault();
        return error;
    }
    else if(fldPassConfirm.value===''){
        error = generateError('Вы забыли подтвердить пароль');
        rPassword.append(error);
        event.preventDefault();
    }
    else if(FPassEnter.value===fldPassConfirm.value){
        if(error!==undefined){
            error.remove();
            error=undefined;
        }
        alert('You are welcome');
    }else{
        if(error===undefined) {
            error = generateError('Нужно ввести одинаковые значения');
            rPassword.append(error);
            event.preventDefault();
        }
    }
}
