export default class Request {
    constructor(url) {
        this.url = url;
    }
    post(entity, data) {
        return axios({
            method: "post",
            url: this.url + entity,
            data: data,
            headers: {
                "Content-Type": "application/json",
            },
        });
    }
    put(entity, id, data) {
        return axios({
            method: "put",
            url: this.url + entity + id,
            headers: {
                "Content-Type": "application/json",
            },
            data: data,
        });
    }
    get(entity) {
        return axios({
            url: this.url + entity,
            headers: {
                "Content-Type": "application/json",
            },
        });
    }
}
