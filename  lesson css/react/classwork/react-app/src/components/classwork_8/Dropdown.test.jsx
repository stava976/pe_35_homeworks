import Dropdown from './Dropdown';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

test('Should render', () => {
  const wrapper = mount(<Dropdown 
    options={[]} 
    onChange={() => {}} 
    selectedItemId={null} />
  );

  expect(wrapper).not.toBeEmptyRender();
});
