import Element from "./Element.js";

export default class Modal {
    constructor(id, classes, text, content) {
        this.id = id;
        this.classes = classes;
        this.text = text;
        this._content = content;
    }
    render(root = document.body) {
        let closeBtn = new Element("span", "&times;", "", [
            "close",
        ]).createElement();

        let fragment = document.createDocumentFragment();
        fragment.append(closeBtn, this._content);

        let modalContent = new Element("div", fragment, "", [
            "modal-content",
        ]).createElement();

        let modal = new Element(
            "div",
            modalContent,
            this.id,
            this.classes
        ).createElement();

        this._modal = modal;
        root.append(modal);

        closeBtn.addEventListener("click", () => {
            modal.classList.remove("active");
        });
    }

    openModal() {
        this._modal.classList.add("active");
    }
}
