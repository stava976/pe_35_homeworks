"use strict";

// const btn = document.getElementById("btn");
// console.log(btn);

// btn.onclick = () => {
//    alert("Hi");
// };

// btn.onclick = () => {
//    alert("Hi again )");
// };

// btn.addEventListener("click", () => {
//    alert("Hi");
// });

// const btnClickHandler = () => {
//    alert("Hi again )");
//    btn.removeEventListener("click", btnClickHandler);
// };
// btn.addEventListener("click", btnClickHandler);

// const onBtnClick = function (event) {
// alert("Hi again 2)");
// console.log(event);
// console.log(this);
// console.log(event.target);
// console.log(this === event.target);
// };
// btn.addEventListener("click", onBtnClick);

// console.log(new Event("click"));

// document.addEventListener("DOMContentLoaded", function (evt) {});

// TASK 1

// const button = document.createElement("button");
// button.textContent = "Enter";
// document.body.append(button);

// const btnHandler = () => {
//    alert("Welcome");
// };
// button.addEventListener("click", btnHandler);

// TASK 2

// const button = document.createElement("button");
// button.textContent = "Enter";
// document.body.append(button);

// const btnHandler = () => {
//    alert("Welcome");
// };
// button.addEventListener("click", btnHandler);

// // button.onmouseover

// const onMouseOver = function () {
//    alert("При клике по кнопке вы войдёте в систему.");
//    button.removeEventListener("mouseover", onMouseOver);
// };

// button.addEventListener("mouseover", onMouseOver);

// TASK 3

const PHRASE = "Добро пожаловать!";

function getRandomColor() {
   const r = Math.floor(Math.random() * 255);
   const g = Math.floor(Math.random() * 255);
   const b = Math.floor(Math.random() * 255);

   return `rgb(${r}, ${g}, ${b})`;
}

const h = document.createElement("h1");
h.textContent = PHRASE;
document.body.append(h);
const button = document.createElement("button");
button.textContent = "Раскрасить";
h.after(button);

const changeColor = function () {
   let hArray = h.textContent.split("").map(function (element) {
      let span = document.createElement("span");
      span.textContent = element;
      span.style.color = getRandomColor();
      return span;
   });
   h.innerHTML = "";

   // hArray.forEach((element) => {
   //    h.append(element);
   // });

   h.append(...hArray);
};

button.addEventListener("click", changeColor);
