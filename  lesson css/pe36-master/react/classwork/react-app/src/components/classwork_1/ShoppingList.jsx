import React from 'react';
import './ShoppingList.css';
import Item from './Item.jsx';
import Counter from './Counter.jsx';
import Form from './Form.jsx'


class ShoppingList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    }
    this.itemInput = React.createRef();
  }

  addItem(value) {
    this.setState({
      items: [...this.state.items, 
        {
          id: new Date().getTime(),
          value,
        }] 
    });
  }

  render() {
    return (
      <div className="shopping-list">
        <h1>Shopping List for Me</h1>
        <div className="wrapper">
          <ul>
            {
              this.state.items.map((socialNetwork) => {
                return <Item text={socialNetwork.value} key={socialNetwork.id} />;
              })
            }
          </ul>
          <Counter count={this.state.items.length} />
        </div>
        <Form onSubmit1={this.addItem.bind(this)}/>
      </div>

    );
  }
}

export default ShoppingList;
