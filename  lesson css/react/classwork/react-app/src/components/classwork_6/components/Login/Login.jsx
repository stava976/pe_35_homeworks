import React, { useContext, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../../containers/UserContainer/UserContainer';


const Login = () => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate(); 
  const {onLogIn} = useContext(UserContext);

  const handleSubmit = (e) => {
    e.preventDefault()
    onLogIn(
      emailRef.current.value, 
      passwordRef.current.value,
      () => {
        navigate('/')
      }
      
    )
  }
  return (
    <>
      <h1>
        Please Login
      </h1>

      <form onSubmit={handleSubmit}>
        <label> Email
          <input type="email" ref={emailRef}/>
        </label>

        <label> Password
          <input type="password" ref={passwordRef}/>
        </label>

        <button type="submit">Submit</button>  
        
      </form>

    </>
  )
  
}


export default Login;
