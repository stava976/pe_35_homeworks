"use strict";

// const str = `"" ''`;
// const str = '""';
// const str = "\u{1F60F} text T";
// console.log(str);
// console.log(str.length);
// console.log(str[4], str[3]);

// console.log(str.toUpperCase());
// console.log(str.toLowerCase());

// console.log(str);

// console.log(str.indexOf("ex"));
// console.log(str.lastIndexOf("ex"));

// console.log(str.indexOf("t"));
// console.log(str.lastIndexOf("t"));

// console.log(str.includes("exe"));

// console.log(str.startsWith("\u{1F60F} "));
// console.log(str.endsWith(" T"));

// console.log(str[3].toUpperCase());

// console.log(str.slice(3, 6));
// console.log(str.slice(3));
// console.log(str.slice()[3].toUpperCase());

// console.log(str.slice(3, 6));

// const newStr = str.slice(3, 6);

// console.log(str.toUpperCase().length);
// console.log(newStr);

// console.log(str.substring(3, 6));
// console.log(str.substr(3, 6));

// console.log(str);
// console.log(str.split("").length);

// console.log("\ud83d" + "\ude0f");

// console.log("a" > "b");

// console.log("a".charCodeAt(0));
// console.log("z".charCodeAt(0));

// console.log("A".charCodeAt(0));
// console.log("Z".charCodeAt(0));

// for (let index = 0; index < 200; index++) {
//    console.log(String.fromCodePoint(index));
// }

// const myDate = new Date("December 17, 1995 03:24:00");

// console.log(myDate);
// console.log(myDate - Date.now());

// console.log(Date.now() / (1000 * 60 * 60 * 24 * 7 * 52));
// console.log(Date.now() / (1000 * 60 * 60 * 24 * 365));
// console.log(new Date(-1000000000));

// console.log(myDate.getFullYear());
// console.log(myDate.setFullYear(2020));
// console.log(myDate);

// console.log(myDate.getFullYear());

//TASK 1
// function getRepeatStr(string, times) {
//    if (
//       typeof string !== "string" ||
//       isNaN(times) ||
//       typeof times !== "number"
//    ) {
//       console.log("No work!");
//    } else {
//       let newStr = "";
//       for (let index = 0; index < times; index++) {
//          newStr += string;
//       }
//       return newStr;
//    }
// }
// console.log(getRepeatStr("null", 5));

// getRepeatStr("null", 5); -> "nullnullnullnullnull"
// "" + null"+"null"+"null"+"null"+"null"

// TASK 2

// function capitalizeAndDoublify(str) {
//    let res = "";
//    for (const el of str) {
//       res = res + el.repeat(2).toUpperCase();
//    }
//    return res;
// }

// let result = capitalizeAndDoublify("hello my beautiful world");
// console.log(result);
// "string".length;

// TASK 3

// function truncate(str, maxLength) {
//    if (str.length > maxLength) {
//       return str.slice(0, maxLength - 3) + "...";
//    }
//    return str;
// }
// console.log(truncate("hello", 4));
