const BASE_URL = "http://localhost:3000";

// deletePost()
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((e) => {
//         console.log(e.message);
//     })
//     .finally(() => {
//         getPosts();
//     });

// const newPost = {
//     title: "New post 2",
//     author: "my post",
//     postText: "lorem ipsum sit amet",
// };

// function getPosts() {
//     axios.get(BASE_URL + "/posts").then(({ data }) => {
//         console.log(data);
//     });
// }

// // createPost(newPost);
// function createPost(post) {
//     return axios({
//         method: "post",
//         url: BASE_URL + "/posts",
//         data: post,
//         headers: {
//             "Content-Type": "application/json",
//         },
//     });
//     //
//     return axios.post(BASE_URL + "/posts", post);
// }

// function deletePost(id = 1) {
//     return axios.delete(BASE_URL + "/posts/" + id);
// }

const formTemplate = document.getElementById("create-post");
console.log(formTemplate);

class Request {
    constructor(url) {
        this.url = url;
    }
    post(entity, data) {
        return axios({
            method: "post",
            url: this.url + entity,
            data: data,
            headers: {
                "Content-Type": "application/json",
            },
        });
    }
}
const request = new Request(BASE_URL);

class Form {
    constructor(formTemplate) {
        this.formTemplate = formTemplate;
    }
    render(root) {
        const fTpl = this.formTemplate.content;
        const form = fTpl.querySelector("form").cloneNode(false);
        console.log(form);
        const title = fTpl.querySelector("#title").cloneNode(false);
        const intro = fTpl.querySelector("#intro").cloneNode(false);
        const text = fTpl.querySelector("#text").cloneNode(false);
        const date = fTpl.querySelector("#date").cloneNode(false);
        const author = fTpl.querySelector("#author").cloneNode(false);
        const submit = fTpl.querySelector('[type = "submit"]').cloneNode(false);

        form.append(title, intro, text, date, author, submit);
        root.append(form);
        form.addEventListener("submit", (e) => {
            e.preventDefault();
            if (
                title.value !== "" &&
                intro.value !== "" &&
                text.value !== "" &&
                date !== "" &&
                author !== ""
            ) {
                const post = {
                    title: title.value,
                    intro: intro.value,
                    text: text.value,
                    date: date.value,
                    author: author.value,
                };
                request
                    .post("/posts", post)
                    .then((e) => {
                        console.log(e);
                    })
                    .catch((er) => {
                        console.log(er);
                    });
            }
        });
    }
}

const root = document.querySelector("#root");
const form = new Form(formTemplate);

form.render(root);
