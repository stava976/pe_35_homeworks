const $tabItems = $(".slider");
const $contentItems = $(".slider-content");

$tabItems.on("click", (event) => {
    $(".slider-tabs").removeClass("slider-tabs");
    $(".active-slider-content").removeClass("active-slider-content");
    $(event.target).addClass("slider-tabs");
    $contentItems.each((index, element) => {
        if (event.target.dataset.type === element.dataset.type){
               return $(element).addClass('active-slider-content');
        }
    });
});
const graph = "graphic-design";
const web = "web-design";
const landing = "landing-pages";
const wordpress = "wordpress";
const hover = "amazing-hover";
const img = "amazing-img";
const portfolioHover = {
    graph: "<div class='amazing-hover'> <div class='amazing-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class='amazing-info'><p>awesome design</p><p>graphic design</p></div></div>",
    web: "<div class='amazing-hover'> <div class='amazing-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class='amazing-info'><p>creative design</p><p>web design</p></div></div>",
    land: "<div class='amazing-hover'> <div class='amazing-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class='amazing-info'><p>variety of patterns</p><p>landing pages</p></div></div>",
    word: "<div class='amazing-hover'> <div class='amazing-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-square'></i></a></div><div class='amazing-info'><p>functional</p><p>wordpress</p></div></div>"
};
const $am = $(".amazing-work");
for (let i = 1; i <= 3; i++) {
    $am.append(`<div class='${img} ${graph}'><img src='img/amazing/graphic-design/graphic-design${i}.jpg' alt='web'></div>`); // загружаем от graphic-design1 до graphic-design3
    $am.append(`<div class='${img} ${web}'><img src='img/amazing/web-design/web-design${i}.jpg' alt='web'></div>`); //загружаем от web-design1 до web-design3
    $am.append(`<div class='${img} ${landing}'><img src='img/amazing/landing-pages/landing-pages${i}.jpg' alt='web'></div>`); //загружаем от landing-pages1 до landing-pages3
    $am.append(`<div class='${img} ${wordpress}'><img src='img/amazing/wordpress/wordpress${i}.jpg' alt='web'></div>`); //загружаем от wordpress1 до wordpress3
}
getImgHover = () => {
    $(".graphic-design>img").before(portfolioHover.graph);
    $(".web-design>img").before(portfolioHover.web);
    $(".landing-pages>img").before(portfolioHover.land);
    $(".wordpress>img").before(portfolioHover.word);
};
let loadBtnStatus = false;
let currentClass = false;
$(".amazing-theme").click((event) => {
    $(".amazing-theme").removeClass("am-active");
    $(event.target).addClass("pam-active");
    currentClass = $(event.target).attr('data-filter');
    if (currentClass !== "all") {
        $(`.${currentClass}`).fadeIn("fast");
        $(".amazing-img").not(`.${currentClass}`).css("display", "none");
    } else {
        $(".amazing-img").css("display", "block");
    }
});
$(".upload-btn").click(() => {
    $(".amazing>.upload-btn").css("display", "block");
    setTimeout(() => {
        $(".amazing>.upload-btn").css("display", "none");
        if (!loadBtnStatus) {
            for (let j = 4; j <= 6; j++) {
                $am.append(`<div class='${img} ${graph}'><img src='img/amazing/graphic-design/graphic-design${j}.jpg' alt='web'></div>`);// загружаем от graphic-design4 до graphic-design6
                $am.append(`<div class='${img} ${web}'><img src='img/amazing/web-design/web-design${j}.jpg' alt='web'></div>`);//загружаем от web-design1 до web-design3
                $am.append(`<div class='${img} ${landing}'><img src='img/amazing/landing-pages/landing-pages${j}.jpg' alt='web'></div>`);//загружаем от landing-pages1 до landing-pages3
                $am.append(`<div class='${img} ${wordpress}'><img src='img/amazing/wordpress/wordpress${j}.jpg' alt='web'></div>`);//загружаем от wordpress1 до wordpress3
                loadBtnStatus = true;
            }
        } else {
            for (let k = 7; k <= 9; k++) {
                $am.append(`<div class='${img} ${graph}'><img src='img/amazing/graphic-design/graphic-design${k}.jpg' alt='web'></div>`);
                $am.append(`<div class='${img} ${web}'><img src='img/amazing/web-design/web-design${k}.jpg' alt='web'></div>`);
                $am.append(`<div class='${img} ${landing}'><img src='img/amazing/landing-pages/landing-pages${k}.jpg' alt='web'></div>`);
                $am.append(`<div class='${img} ${wordpress}'><img src='img/amazing/wordpress/wordpress${k}.jpg' alt='web'></div>`);
                $(".upload-btn").hide();
            }
        }
        if (currentClass !== "all" && currentClass !== false) {
            $(`.${currentClass}`).css("display", "block");
            $(".amazing-img").not(`.${currentClass}`).css("display", "none");
        }
        getImgHover();
    }, 4000);
});
getImgHover();
$('.classForSlick').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
});
$('.classForSlick2').slick({
    dots: false,
    centerMode: true,
    arrows:true,
    prevArrow: '.myNawLeft',
    nextArrow: '.myNawRight',
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    // slidesToShow: 4,
    adaptiveHeight: true,
    asNavFor:'.classForSlick',
    focusOnSelect: true,
    variableHeight: true,
    variableWidth: true
});
document.addEventListener("DOMContentLoaded",function(){
    let btn = this.getElementById("upload");

    if (btn) {
        btn.addEventListener("click",function(){
            let cl = this.classList,
                r = "upload-btn--running",
                d = "upload-btn--done",
                dur = 3000;

            if (!cl.contains(r) && !cl.contains(d) && !this.disabled) {
                cl.add(r);
                this.disabled = true;
                this.innerHTML = "Uploading…";

                setTimeout(() => {
                    cl.remove(r);
                    cl.add(d);
                    this.innerHTML = "loaded!";
                },dur);
            }
        });
    }
});
