const URI = "https://ajax.test-danit.com/api/swapi/planets";

// function getPlanets() {
//     return new Promise((resolve, reject) => {
//         const xhr = new XMLHttpRequest();
//         xhr.open("GET", URI);
//         xhr.send(null);

//         xhr.onreadystatechange = function () {
//             if (xhr.readyState == 4) {
//                 if (xhr.status == 200) {
//                     const response = JSON.parse(xhr.response);
//                     resolve(response);
//                 } else {
//                     reject(new Error("Error loading\n"));
//                 }
//             }
//         };
//     });
// }

// getPlanets()
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((e) => {
//         console.log(e);
//     })
//     .finally(() => {
//         console.log("finally");
//     });

///////// FETCH
// function showAvatar() {
//     return fetch(`https://api.github.com/users/telegraf`, {
//         method: "GET",
//         headers: {
//             "Content-Type": "application/json",
//         },
//     });
// }

// showAvatar()
//     .then((response) => {
//         console.log(response);

//         // return  response.text();
//         //   return response.blob();
//         return response.json();
//     })
//     .then((data) => {
//         console.log(data);
//         const img = document.createElement("img");
//         img.src = data.avatar_url;
//         document.body.prepend(img);
//     })
//     .catch((e) => {
//         console.log(e.message);
//     });

// axios({
//  method: "GET",
//  url: "https://ajax.test-danit.com/api/swapi/films",
// }).then(function (response) {
// }).then(function ({ data, status, statusText, headers, config }) {
//     console.log(data);
//     console.log(status);
//   console.log(statusText);
//   console.log(headers);
//   console.log(config);
//  console.log(response);
// });

// axios
//     .get("https://ajax.test-danit.com/api/swapi/films", {})
//     .then(function ({ data }) {
//         console.log(data);
//     });

// TASK 1

// class Film {
//     constructor(url) {
//         this.url = url;
//     }

//     getList(event) {
//         event.preventDefault();
//         const { value: planetID } = document.querySelector("#planet");
//         const root = document.querySelector("#root");

//         axios.get(this.url).then(function ({ data }) {
//             const res = data.find(function ({ id }) {
//                 return +planetID === id;
//             });
//             for (const key in res) {
//                 root.innerHTML += `<strong>${key}</strong>: ${res[key]}<br>`;
//             }
//         });
//     }
// }

// const form = document.querySelector("form");
// const url = "https://ajax.test-danit.com/api/swapi/planets";
// const film = new Film(url);
// form.addEventListener("submit", film.getList.bind(film));

// TASK 2
const url = "https://ajax.test-danit.com/api/swapi/people";
class Hero {
    constructor(url) {
        this.url = url;
    }
    planetData() {
        let select = document.createElement("select");
        document.getElementById("root").before(select);
        let heroObj;

        select.addEventListener("change", () => {
            document.getElementById("root").innerHTML = "";

            const res = heroObj.find(function ({ name }) {
                return select.value === name;
            });
            axios.get(res.homeworld, {}).then(function ({ data }) {
                for (const key in data) {
                    let p = document.createElement("p");
                    p.innerHTML = `<strong>${key}</strong>: ${data[key]}<br>`;
                    document.getElementById("root").append(p);
                }
            });
        });

        axios.get(this.url, {}).then(function ({ data }) {
            data.forEach(({ name }) => {
                let option = document.createElement("option");
                option.textContent = name;
                option.value = name;
                select.append(option);
            });
            heroObj = data;
        });
    }
}
const hero = new Hero(url);
hero.planetData();
