import React from 'react';

import {ContainerHeader, StyledNavLink} from './StyledHeader';

export const Header = () => {
    return (

        <ContainerHeader>
            <StyledNavLink to='/'>Главная</StyledNavLink>
            <StyledNavLink to='/favorite'>Избранное</StyledNavLink>
            <StyledNavLink to='/basket'>Корзина</StyledNavLink>
        </ContainerHeader>

    )
}
