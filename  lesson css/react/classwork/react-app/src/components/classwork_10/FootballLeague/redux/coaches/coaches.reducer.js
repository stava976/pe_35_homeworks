import { FETCH_COACHES_SUCCESS } from "./coaches.actions"

const initialState = {
  coachesList: [],
}

const coachesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COACHES_SUCCESS:
      return {
        ...state,
        coachesList: action.payload
      }

    default:
      return state
  }
}

export default coachesReducer