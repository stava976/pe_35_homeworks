import { ADDED_TEAM_ERROR, ADDED_TEAM_REQUEST, ADDED_TEAM_SUCCESS, FETCH_TEAMS_REQUESTED, FETCH_TEAMS_SUCCESS, TEAM_DELETED_REQUEST, TEAM_DELETED_SUCCESS, TEAM_EDIT_ERROR, TEAM_EDIT_REQUESTED, TEAM_EDIT_STARTED, TEAM_EDIT_SUCCESS, TEAM_UPDATED } from "./teams.actions";

const initialState = {
  editRequestState: 'idle',
  fetchRequestState: 'idle',
  addRequestState: 'idle',
  deleteRequestState: 'idle',
  teamToEditId: null,
  teamsList: [],
}

const teamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADDED_TEAM_REQUEST: {
      return {
        ...state,
        addRequestState: 'loading',
      };
    }

    case ADDED_TEAM_ERROR: {
      return {
        ...state,
        addRequestState: 'error',
      };
    }

    case ADDED_TEAM_SUCCESS:
      const { name, city, coach } = action.payload;

      return {
        ...state,
        addRequestState: 'success',
        teamsList: [...state.teamsList, {
          id: state.teamsList.length + 1,
          name,
          city,
          coach,
        }],
      };

    case TEAM_EDIT_STARTED: {
      return {
        ...state,
        teamToEditId: action.payload,
      }
    }

    case TEAM_UPDATED: {
      const updatedTeam = action.payload;
      const newTeams = state.teamsList.map((team) => {
        if(team.id === updatedTeam.id) {
          return updatedTeam;
        }
        return team
      })
      return {
        ...state, 
        teamsList: newTeams,
        teamToEditId: null
      }
    }

    case FETCH_TEAMS_REQUESTED: {
      return {
        ...state,
        fetchRequestState: 'loading',
      }
      
    }
    
    case FETCH_TEAMS_SUCCESS: {
      console.log(action);
      return {
        ...state,
        fetchRequestState: 'success',
        teamsList: action.payload,
      }
    }

    case TEAM_EDIT_REQUESTED: {
      return {
        ...state,
        editRequestState: 'loading',
      }
    }

    case TEAM_EDIT_SUCCESS: {
      return {
        ...state,
        editRequestState: 'success',
        teamToEditId: null,
        teamsList: state.teamsList.map(team => {
          if (team.id === action.payload.id) {
            return action.payload
          } else {
            return team
          }
        })
      }
    }

    case TEAM_EDIT_ERROR: {
      return {
        ...state,
        editRequestState: 'error',
      }
    }

    case TEAM_DELETED_REQUEST:
      return {
        ...state,
        deleteRequestState: 'loading',
      }

    case TEAM_DELETED_SUCCESS:
      return {
        ...state,
        deleteRequestState: 'success',
        teamsList: state.teamsList.filter((team) => +team.id !== +action.payload)
      }


    default:
      return state;
  }
}

export default teamsReducer;
