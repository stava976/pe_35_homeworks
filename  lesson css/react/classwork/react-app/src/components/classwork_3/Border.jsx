import React from 'react'
import './Border.css'

class Border extends React.Component {
  componentDidMount() {
    console.log('componentDidMount');
    document.addEventListener('scroll', this.handleScroll); 
  }

  handleScroll() {
    console.log('Scroll!');
  }
  componentWillUnmount() {
    console.log('componentWillUnmount');
    document.removeEventListener('scroll', this.handleScroll); 
  }
  render() {
    return (
    <span className="border">{this.props.children}</span>
    )
  }
}

export default Border