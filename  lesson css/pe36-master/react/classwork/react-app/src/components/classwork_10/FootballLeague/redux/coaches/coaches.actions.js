const API_SERVER = 'http://localhost:3001';

export const FETCH_COACHES_REQUESTED = 'FETCH_COACHES_REQUESTED';
export const fetchCoachesRequested = () => ({
  type: FETCH_COACHES_REQUESTED,
});

export const FETCH_COACHES_SUCCESS = 'FETCH_COACHES_SUCCESS';
export const fetchCoachesSuccess = (coaches) => ({
  type: FETCH_COACHES_SUCCESS,
  payload: coaches,
});

export const FETCH_COACHES_ERROR = 'FETCH_COACHES_ERROR';
export const fetchCoachesError = () => ({
  type: FETCH_COACHES_ERROR,
});

export const fetchCoaches = () => {
  return (dispatch) => {
    dispatch(fetchCoachesRequested());
    fetch(`${API_SERVER}/coach`, {
      method: 'GET',
    })
      .then((response) => response.json())
      .then(coaches => {
        dispatch(fetchCoachesSuccess(coaches));
      })
      .catch(e => {
        dispatch(fetchCoachesError());
      });
  }
} 