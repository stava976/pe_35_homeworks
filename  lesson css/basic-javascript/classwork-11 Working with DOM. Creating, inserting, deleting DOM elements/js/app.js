"use strict";

const root = document.getElementById("root");
// const paragraph = document.createElement("p");

// console.log(paragraph);

// paragraph.classList.add("text");
// paragraph.style.fontSize = "30px";
// paragraph.textContent = "lorem ipsum sit amet";
// paragraph.setAttribute("id", "big-text");

// paragraph.setAttribute("class", "big-text");

// paragraph.setAttribute("data-number", "1");
// // paragraph.setAttribute("data-size", "30px");
// paragraph.dataset.size = "30px";

// console.log(paragraph.getAttribute("id"));

// root.append(paragraph);
// root.prepend(paragraph);
// root.after(paragraph);
// root.before(paragraph);

// paragraph.remove();

// root.before("<strong>strong here</strong>");

// root.insertAdjacentHTML("afterbegin", "<strong>strong here</strong>");
// root.insertAdjacentHTML("afterbegin", paragraph);
// root.insertAdjacentElement("afterbegin", paragraph);
// root.insertAdjacentText("afterbegin", "<strong>strong here</strong>");

// paragraph.classList.add();
// paragraph.classList.remove();
// paragraph.classList.toggle();
// console.log(paragraph.classList.contains("big-text"));

// const fragment = document.createDocumentFragment();
// console.log(fragment);

// for (let index = 0; index < 5; index++) {
//    const paragraph = document.createElement("p");
//    paragraph.classList.add("text");
//    paragraph.style.fontSize = "30px";
//    paragraph.textContent = "lorem ipsum sit amet";

//    fragment.append(paragraph);
// }
// console.log(fragment);

// root.append(fragment);
// console.log(fragment);

// TASK 1

// let size;
// do {
//    size = prompt("size");
// } while (
//    typeof +size !== "number" ||
//    isNaN(+size) ||
//    size.trim() === "" ||
//    size <= 0
// );

// const div = document.createElement("div");
// div.style.cssText = `width: ${size}px; height: ${size}px; background-color: red`;

// root.innerHTML = "";

// div.classList.add("square");
// root.prepend(div);

// TASK 2

function createSquares(numberSquares) {
   for (let i = 0; i < numberSquares; i++) {
      let size;
      do {
         size = prompt("Enter size");
      } while (!checkNumber(size));

      let error = 0;
      do {
         let color = prompt("Enter color in rgb:0-255.0-255.0-255");
         let colorarray = color.split(".");
         error = 0;
         colorarray.forEach((element) => {
            if (!checkNumber(element) || element < 0 || element > 255) {
               error++;
            }
         });
         console.log(error);
      } while (error !== 0);
   }
}
let numberSquares;
do {
   numberSquares = prompt("numberSquares");
   if (numberSquares >= 10) {
      console.log("NO!");
      continue;
   }
} while (
   !checkNumber(numberSquares) ||
   numberSquares <= 0 ||
   numberSquares >= 10
);

function checkNumber(number) {
   if (typeof +number !== "number" || isNaN(+number) || number.trim() === "") {
      return false;
   }
   return true;
}
createSquares(numberSquares);
