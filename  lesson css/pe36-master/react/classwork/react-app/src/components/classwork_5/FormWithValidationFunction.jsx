import React, { useEffect, useRef, useState } from 'react';
import './FormWithValidation.css';
import Input from './Input';

const emailRegexp = /(.+)@(.+){2,}\.(.+){2,}/;
const validateEmail = (email) => {
  return emailRegexp.test(email);
}

function FormWithValidationFunction () {
  const [email, setEmail] = useState('');
  const [isEmailInvalid, setIsEmailInvalid] = useState(false);

  const onEmailChange = (e) => {
    setEmail(e.target.value);
    setIsEmailInvalid(!validateEmail(e.target.value));
  }

  const [password, setPassword] = useState('');
  const [isPasswordInvalid, setIsPasswordInvalid] = useState(false);

  const onPasswordChange = (e) => {
    setPassword(e.target.value);
    setIsPasswordInvalid(e.target.value === '');
  }

  const [loading, setLoading] = useState(false);
  const onSubmit = () => {
    setLoading(true);
  }

  if (loading) return <div>Loading...</div>;

  return (
    <div>
      <h1>Login</h1>
      <form>
        <Input 
          value={email}
          onChange={onEmailChange}
          label="Email"
          error={
            isEmailInvalid 
              ? 'Please, enter valid email'
              : null
            }
        />

        <Input 
          value={password}
          onChange={onPasswordChange}
          label="Password"
          error={
            isPasswordInvalid 
              ? 'Please, enter password'
              : null
            }
        />

        <button onClick={onSubmit}>Submit</button>
      </form>
    </div>
  );
}

export default FormWithValidationFunction;
