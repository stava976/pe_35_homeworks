import React, { useEffect, useState } from 'react';

function Students () {
  const [students, setStudents] = useState([]);

  useEffect(() => {
    fetch('https://ebdf-217-25-198-197.ngrok.io/student')
      .then(response => response.json())
      .then(students => {
        setStudents(students);
      });
  }, []);

  useEffect(() => {
    const newStudents = [];
    students.forEach((student) => {
      fetch(`https://ebdf-217-25-198-197.ngrok.io/teacher/${student.teacherId}`)
        .then(response => response.json())
        .then(teacher => newStudents.push({ ...student, teacher }));
    });

    // setStudents(newStudents);
  }, [students]);

  return (
    <div>
      <ul>
        {
          students.map((student) => {
          return <li key={student.id}>{student.firstName} {student.lastName}</li>
          })
        }
      </ul>
    </div>
  );
}

export default Students;
