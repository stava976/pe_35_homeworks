import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchCoaches } from '../../redux/coaches/coaches.actions'

const Coaches = () => {
  const coaches = useSelector(state => state.coaches.coachesList)

  const [coachToEdit, setCoachToEdit] = useState(null)
  const dispatch = useDispatch()


  useEffect(() => {
    
    dispatch(fetchCoaches())
  }, [])

  return (
    <div>
      {
        coaches.map((coach) => {
          return (<div key={coach.id} style={{ margin: '18px 12px 12px' }}>
            { 
              coachToEdit?.id === coach.id ? 
              <>
                Edit
              </> :
              <>
                Coach name: <h3 style={{ margin: '0 0 10px' }}>{coach.name}</h3>
                age: {coach.age} <br/>
                nationality: {coach.nationality} <br />
                {/* <button data-id={team.id} onClick={handleEditRequest}>Edit</button> */}
                {/* <button data-id={team.id} onClick={handleDeleteRequest}>Delete</button> */}
              </>
            }

          </div>);
        })
      }
    </div>
  )
}

export default Coaches