import React from "react";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.itemInput = React.createRef();
  }

  onClick() {
    const input = this.itemInput.current;
    this.props.onSubmit1(input.value)
  }

  render() {
    return (
      <>
        <input ref={this.itemInput}/>
        <button onClick={this.onClick.bind(this)}>Submit</button>
      </>
    )
  }
}

export default Form;