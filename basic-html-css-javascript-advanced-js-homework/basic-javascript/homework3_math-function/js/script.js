"use strict";
function calcResult(Number1, Number2, Operation) {
    Number1 = prompt("Please enter a first Number", '50');
    while (isNaN(Number1) || Number1 === '' || Number1 === null || !Number1) {
        Number1 = prompt("Please enter a first Number")
    }
    Number2 = prompt("Please enter a second Number", '50');
    while (isNaN(Number2) || Number2 === '' || Number2 === null || !Number2 ) {
        Number2 = prompt("Enter the second number again")
    }
    Operation = prompt("Enter mathematical operation", '+ - * / < >');
    while (Operation !== '+' && Operation !== '-' && Operation !== '*' && Operation !== '/' && Operation !== '<' && Operation !== '>') {
        Operation = prompt("The math operation was entered incorrectly. Introduced -" + " " + "'" + Operation + "'" + "! re-enter")
    }
    switch (Operation) {
        case '+':
            return +(Number1) + +(Number2);
        case '-':
            return Number1 - Number2;
        case '*':
            return Number1 * Number2;
        case '/':
            return Number1 / Number2;
        case '<':
            return Number1 < Number2;
        case '>':
            return Number1 > Number2;
    }
}
console.log(calcResult());

