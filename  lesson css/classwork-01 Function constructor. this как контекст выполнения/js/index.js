"use strict";

// const obj = {};

// const name = "Uasya";
// const age = 33;
// Object.create();

// function objectCreate(...params) {
//     return { ...params };
// }

// console.log(objectCreate(name, age));

// function ObjectCreate(propName = "Empty", propAge = 18) {
//     this.name = propName;
//     this.age = propAge;

//     this.showName = function () {
//         console.log(this.name);
//     };
// let newAge = 33;
// const newName = "Petya";
// return {
//     is: false,
// };
// }
// const user = new ObjectCreate("Uasya", 33);
// const obj2 = new ObjectCreate("Uasya2");

// const user1 = {
//     name: "Petya",
// };
// for (const key in user) {
//     if (Object.hasOwnProperty.call(user, key)) {
//         const element = user[key];
//         console.log(element);
//     }
// }

// console.log("12".hasOwnProperty("2"));
// console.log(user);
// bind, call, apply
// user1.showName = user.showName.bind(user1);
// user1.showName();

// user.showName.bind(user1)();
// user.showName();

// user.showName.call(user1);
// user.showName.apply(user1);

// console.log(obj2);
// user.showName();

// const arr = new Array(1, 2, 3, 4, 5);
// console.log(arr);
// console.log(typeof arr);

// arr.forEach((el) => {
//     console.log(el);
// });

// const str = new String(123);
// console.log(str);
// console.log(str.split());
// console.log(typeof str);
// console.log("123".split());
