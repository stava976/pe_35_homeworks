"use strict";
const input = document.getElementById('number');
const divPrice = document.getElementById('price');
input.onfocus = function () {
    input.classList.add('focus');
    if (this.classList.contains('invalid')){
        this.classList.remove('invalid');
        error.innerHTML = '';
    }
};
input.onblur = function () {
    if (input.value <= 0){
        input.classList.add('invalid');
        error.innerHTML = 'Please enter correct price.';
        return;
    }
    const textPrice = document.createElement('span');
    textPrice.innerText = `Текущая цена ` + input.value;
    divPrice.appendChild(textPrice);
    const buttonX = document.createElement('button');
    buttonX.innerText = `x`;
    buttonX.classList.add('price-esc');
    textPrice.appendChild(buttonX);
    input.style.color = 'green';
    buttonX.addEventListener('click', () => {
        textPrice.hidden = true;
        buttonX.hidden = true;
        input.value = '';
    })
};
