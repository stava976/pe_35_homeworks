import { NavLink } from 'react-router-dom';
import styled from 'styled-components';


export const ContainerHeader = styled.header`
display: flex;
justify-content: space-around;
align-items:center;
padding: 1.5rem 0 1.5rem;
border-bottom: .5rem solid #348672;
`;

export const StyledNavLink = styled(NavLink)`
font-size: 2rem;
font-weight: 700;
text-decoration: none;
width: 17rem;
padding: 1rem 0;
border: .1rem solid #273ca3;
background: #273ca3;
color: #FFFFFF;
border-radius: 1.9rem;
text-align: center;
transition: all .3s linear;
&:hover{
transform:scale(1.2);
}
`;