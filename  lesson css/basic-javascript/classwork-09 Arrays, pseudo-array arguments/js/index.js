"use strict";

const arr = [1, 12, 33, 4, 65];
// console.log(arr);

// console.log(arr);
// console.log(typeof arr);

// arr.push(5, 5, 6);
// console.log(arr);

// arr.pop();
// console.log(arr);

// arr.unshift(5, 5, 6);
// console.log(arr);

// arr.shift();
// console.log(arr);

// arr.reverse();
// console.log(arr);

// console.log("31.12.1999".split(".").reverse().join("."));

// console.log(arr.length);
// console.log(arr[0]);

// console.log(arr.indexOf(2, 3));
// console.log(arr.lastIndexOf(2, 3));
// console.log(arr.includes(9));

// for (let index = 0; index < arr.length; index++) {
//    console.log(arr[index]);
// }

// for (const value of arr) {
//    console.log(value);
// }

// console.log(arr.concat([0, 9, 8], 100, 99, 88));
// console.log(arr);

// arr.length = 10;
// console.log(arr);

// arr[5] = 5;
// arr.push(5, 5, 5, 5, 5);

// arr[20] = 20;
// console.log(arr);

// delete arr[3];
// console.log(arr);
let res;

// res = arr.slice(2, 4);
// console.log(res);
// console.log(arr);

// res = arr.splice(2, 0, 9, 9, 9, 9, 9);
// console.log(res);
// console.log(arr);

// function callBackFn(value, index, array) {
//    console.log(`${index}: ${value}, - ${array}`);
// }

// arr.forEach(callBackFn);

// arr.forEach(function (value, index, array) {
//    console.log(`${index}: ${value}, - ${array}`);
// });

// arr.forEach((value, index, array) => {
//    console.log(`${index}: ${value}, - ${array}`);
// });

// arr.reverse();
// console.log(arr);

// arr.sort((a, b) => b - a);
// console.log(arr);
const users = [
   {
      age: 33,
   },
   {
      age: 12,
   },
   {
      age: 18,
   },
   {
      age: 26,
   },
   {
      age: 30,
   },
   {
      age: 6,
   },
];

// res = users.find((value, index, array) => value.age === 25);
// console.log(res);

// res = users.findIndex((value, index, array) => value.age > 30);
// console.log(res);

// res = users.filter((value, index, array) => value.age > array.length);
// console.log(res);

// res = users.map((value, index, array) => value.age + " years");
// console.log(res);
// res = 0;
// users.forEach((el) => {
//    res += el.age;
// });
// console.log(res);

// res = users.reduce(
//    (startValue, currentValue, index, array) => startValue + currentValue.age,
//    0
// );
// console.log(res);

// res = users.reduceRight(
//    (startValue, currentValue, index, array) => startValue + currentValue.age,
//    0
// );
// console.log(res);

// TASK 1
// const days = [
//    "Monday",
//    "Tuesday",
//    "Wednesday",
//    "Thursday",
//    "Friday",
//    "Saturday",
//    "Sunday",
// ];

// for (let index = 0; index < days.length; index++) {
//    console.log(days[index]);
// }

// for (const iterator of days) {
//    console.log(iterator);
// }

// days.forEach((value, index, array) => {
//    console.log(`${value}`);
// });

// TASK 2
// const days = {
//    ua: [
//       "Понеділок",
//       "Вівторок",
//       "Середа",
//       "Четвер",
//       "П’ятниця",
//       "Субота",
//       "Неділя",
//    ],
//    ru: [
//       "Понедельник",
//       "Вторник",
//       "Среда",
//       "Четверг",
//       "Пятница",
//       "Суббота",
//       "Воскресенье",
//    ],
//    en: [
//       "Monday",
//       "Tuesday",
//       "Wednesday",
//       "Thursday",
//       "Friday",
//       "Saturday",
//       "Sunday",
//    ],
// };

// function getWeek(days) {
//    let lang = " ";
//    while (!Object.keys(days).includes(lang)) {
//       lang = prompt("Enter lang: ");
//    }
//    days[lang].forEach((element) => {
//       console.log(element);
//    });
// }
// getWeek(days);

// TASK 3

function mergeArrays(...params) {
   let newArr = [];
   params.forEach((element, index) => {
      if (!Array.isArray(element)) {
         console.log(`Error in ${index}`);
         return false;
      }
   });

   newArr = newArr.concat(...params);
   return newArr;
}
console.log(mergeArrays(1, [8]));
