"use strict";
const button = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (e) {
    button.forEach((el) => {
        el.style.backgroundColor = 'black';
        if (e.key.toLowerCase() === el.outerText.toLowerCase()) {
            el.style.backgroundColor = '#4BD5EE';
        }
    });
});
