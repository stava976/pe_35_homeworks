"use strict";
let bodyStyle = document.body.style,
    dayBtn = document.getElementById('day'),
    nightBtn = document.getElementById('night'),
    textTheme = document.querySelector('section').style,
    h2textTheme = document.querySelector('h2').style,
    ptextTheme = document.querySelector('p').style,
    dayColor = '#eceff1',
    nightColor = '#424242';
window.onload = function() {
    localStorage.getItem("checked");
    let checked = localStorage["checked"];
    if (checked === 'day') {
        bodyStyle.backgroundColor = dayColor;
        textTheme.color = nightColor;
        h2textTheme.color = nightColor;
        ptextTheme.color = nightColor;
        dayBtn.checked = true;
        nightBtn.checked = false;
    } else {
        bodyStyle.backgroundColor = nightColor;
        textTheme.color = dayColor;
        h2textTheme.color = dayColor;
        ptextTheme.color = dayColor;
        dayBtn.checked = false;
        nightBtn.checked = true;
    }
}
function switchBg() {
    if (dayBtn.checked === true) {
        bodyStyle.backgroundColor = dayColor;
        textTheme.color = nightColor;
        h2textTheme.color = nightColor;
        ptextTheme.color = nightColor;
        localStorage["checked"] = "day";
    } else {
        bodyStyle.backgroundColor = nightColor;
        textTheme.color = dayColor;
        h2textTheme.color = dayColor;
        ptextTheme.color = dayColor;
        localStorage["checked"] = "night";
    }
}

