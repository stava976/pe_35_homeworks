import React from 'react';
import { Provider } from 'react-redux';
import { combineReducers } from 'redux';
import Teams from './components/Teams/Teams';
import { loggerMiddleware } from './redux/logger.middleware';
import teams from './redux/teams/teams.reducer.js';
import coaches from './redux/coaches/coaches.reducer.js';
import user from './redux/user/user.reducer';
import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import Coaches from './components/Coaches/Coaches';
import CreateTeam from './components/CreateTeam/CreateTeam';

const reduxDevToolsCompose = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'];

const rootReducer = combineReducers({
  teams,
  user,
  coaches,
});

const store = configureStore({
  devTools: reduxDevToolsCompose,
  reducer: rootReducer,
  middleware: [thunk, loggerMiddleware],
});

const FootballLeageApp = () => {
  return (
    <Provider store={store}>
      <Teams />
      <CreateTeam />
      <Coaches />
    </Provider>
  );
}

export default FootballLeageApp;
