export const ADDED_TEAM_REQUEST = 'ADDED_TEAM_REQUEST';
export const addedTeamRequest = (newTeamData) => ({
  type: ADDED_TEAM_REQUEST,
  payload: newTeamData,
});

export const ADDED_TEAM_SUCCESS = 'ADDED_TEAM_SUCCESS';
export const addedTeamSuccess = (team) => ({
  type: ADDED_TEAM_SUCCESS,
  payload: team,
});

export const ADDED_TEAM_ERROR = 'ADDED_TEAM_ERROR';
export const addedTeamError = () => ({
  type: ADDED_TEAM_ERROR,
});

const API_SERVER = 'http://localhost:3001';
export const addTeam = (newTeamData) => {
  return (dispatch) => {
    dispatch(addedTeamRequest(newTeamData));
    fetch(`${API_SERVER}/team`, {
      method: 'POST',
      headers: {'Content-type' : 'application/json'},
      body: JSON.stringify(newTeamData),
    })
      .then((response) => response.json())
      .then(team => {
        dispatch(addedTeamSuccess(team));
      })
      .catch(e => {
        dispatch(addedTeamError());
      });
  }
} 
export const FETCH_TEAMS_REQUESTED = 'FETCH_TEAMS_REQUESTED';
export const fetchTeamsRequested = () => ({
  type: FETCH_TEAMS_REQUESTED,
});

export const FETCH_TEAMS_SUCCESS = 'FETCH_TEAMS_SUCCESS';
export const fetchTeamsSuccess = (teams) => ({
  type: FETCH_TEAMS_SUCCESS,
  payload: teams,
});

export const FETCH_TEAMS_ERROR = 'FETCH_TEAMS_ERROR';
export const fetchTeamsError = () => ({
  type: FETCH_TEAMS_ERROR,
});


export const fetchTeams = () => {
  return (dispatch) => {
    dispatch(fetchTeamsRequested());
    fetch(`${API_SERVER}/team`, {
      method: 'GET',
    })
      .then((response) => response.json())
      .then(teams => {
        dispatch(fetchTeamsSuccess(teams));
      })
      .catch(e => {
        dispatch(fetchTeamsError());
      });
  }
} 

export const TEAM_EDIT_REQUESTED = 'TEAM_EDIT_REQUESTED';
export const teamEditRequested = (team) => ({
  type: TEAM_EDIT_REQUESTED,
  payload: team,
});

export const TEAM_EDIT_ERROR = 'TEAM_EDIT_ERROR';
export const teamEditError = () => ({
  type: TEAM_EDIT_ERROR,
});

export const TEAM_EDIT_SUCCESS = 'TEAM_EDIT_SUCCESS';
export const teamEditSuccess = (team) => ({
  type: TEAM_EDIT_SUCCESS,
  payload: team,
});

export const editTeam = (teamData) => {
  return (dispatch) => {
    dispatch(teamEditRequested(teamData));
    fetch(`${API_SERVER}/team/${teamData.id}`, {
      method: 'PATCH',
      headers: {'Content-type' : 'application/json'},
      body: JSON.stringify(teamData),
    })
      .then((response) => response.json())
      .then(team => {
        dispatch(teamEditSuccess(team));
      })
      .catch(e => {
        dispatch(teamEditError());
      });
  }
} 

export const TEAM_EDIT_STARTED = 'TEAM_EDIT_STARTED';
export const teamEditStarted = (teamId) => ({
  type: TEAM_EDIT_STARTED,
  payload: teamId
});

export const TEAM_DELETED_REQUEST = 'TEAM_DELETED_REQUEST';
export const teamDeletedRequest = (team) => {
  return {
    type: TEAM_DELETED_REQUEST,
    payload: team,
  }
}

export const TEAM_DELETED_SUCCESS = 'TEAM_DELETED_SUCCESS';
export const teamDeletedSuccess = (team) => {
  return {
    type: TEAM_DELETED_SUCCESS,
    payload: team,
  }
}

export const TEAM_DELETED_ERROR = 'TEAM_DELETED_ERROR';
export const teamDeletedError = () => {
  return {
    type: TEAM_DELETED_ERROR,
  }
}

export const deleteTeam = (id) => {
  return (dispatch) => {
    dispatch(teamDeletedRequest(id));
    fetch(`${API_SERVER}/team/${id}`, {
      method: 'DELETE',
    })
      .then(() => {
        dispatch(teamDeletedSuccess(id));
      })
      .catch(e => {
        dispatch(teamDeletedError());
      });
  }
}

export const TEAM_UPDATED = 'TEAM_UPDATED';