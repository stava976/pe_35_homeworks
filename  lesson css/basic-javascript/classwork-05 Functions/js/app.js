"use strict";

// function fnName(){}

// const a = 3;
// const b = 4;

// console.log(typeof 4);
// console.log(summ(9, a));
// let result = summ(4, 1);

// function outer(a, b) {
//    function summ(a, b) {
//       // your code
//       return a + b;
//       console.log("Done!");
//    }
//    console.log(summ(a, b));
// }

// function summ(a, b) {
//    // your code

//    // if (false) {
//    //    return a + b;
//    // } else {
//    //    return "OOps!";
//    // }

//    if (true) {
//       return a + b;
//    }
//    return "OOps!";

//    console.log("Done!");
// }

// const summ2 = function anyName(a, b) {
//    // console.log(arguments);
//    // console.log(arguments instanceof Array);
//    // console.log([] instanceof Array);
//    // console.log(arguments.length);
//    // console.log(arguments[3]);
//    if (a) {
//       console.log(anyName(0, 1));
//       a = 0;
//    }
//    return a + b;
// };

// console.log(summ2(7, 3, 4, 5, 6, 7, 8, 9));

// TASK 1
// const a = 5;
// const b = 10;

// function summ(a, b) {
//    return a + b;
// }
// console.log(summ(a, b));

// task 2

// function getLength() {
//    return arguments.length;
// }
// console.log(getLength(2, 1, 67, 89));

//////// TASK3

// function count(a, b) {
//    if (b < a) {
//       console.log(`«⛔️ Ошибка! Счёт невозможен.»`);
//    } else if (b === a) {
//       console.log(`«⛔️ Ошибка! Нечего считать.»`);
//    } else {
//       console.log(`«🏁 Отсчёт начат.».`);

//       for (let i = a; i <= b; i++) {
//          console.log(i);
//       }

//       console.log(`«✅ Отсчёт завершен.»`);
//    }
// }
// count(6, 10);

// TASK 4

// function countAdvanced(a, b, c) {
//    if (arguments.length !== 3) {
//       console.log("NO c argument");
//    }
//    if (
//       isNaN(a) ||
//       isNaN(b) ||
//       isNaN(c) ||
//       !a ||
//       !b ||
//       !c ||
//       !Number(a) ||
//       !Number(b) ||
//       !Number(c)
//    ) {
//       console.log("Goodbye");
//    } else if (b < a) {
//       console.log(`«⛔️ Ошибка! Счёт невозможен.»`);
//    } else if (b === a) {
//       console.log(`«⛔️ Ошибка! Нечего считать.»`);
//    } else {
//       console.log(`«🏁 Отсчёт начат.».`);

//       for (let i = a; i <= b; i++) {
//          if (i % c === 0) {
//             console.log(i);
//          }
//       }

//       console.log(`«✅ Отсчёт завершен.»`);
//    }
// }

// countAdvanced(0, 10, 3);

// TASK 5
function summ() {
   let result = 0;
   for (let index = 0; index < arguments.length; index++) {
      if (
         isNaN(arguments[index]) ||
         !arguments[index] ||
         !Number(arguments[index])
      ) {
         // return new Error(`Error in ${index} element`);
         return `Error in ${index} element`;
      } else {
         result += arguments[index];
      }
   }

   return result;
}

console.log(summ(1, 2, 3, 4, 8, 0));
