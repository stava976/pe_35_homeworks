const { watch, parallel } = require("gulp");
const { bs } = require("./serv.js");

const watcherFn = () => {
   watch("./index.html").on("change", bs.reload);
   watch("./src/js/*.js").on("change", parallel(scripts));
};

exports.watcher = watcherFn;
