"use strict";

// BOM - browser object model
// console.log(window);
// console.log(navigator);

// JSOM, CSSOM

// DOM - document object model
// console.log(document);
// console.log(document.body);
// console.log(document.body.childNodes); // NodeList
// console.log(document.body.children); // HTMLCollection
// console.log(document.body.children.list);
// console.log(document.body.children[0]);

// console.log(document.body.childNodes[0]);

// const list = document.getElementById("list");
// console.log(list);

// const list2 = document.getElementsByClassName("listss");
// console.log(list2);

// const listItems = document.getElementsByTagName("li");
// console.log(listItems);

// const listItems2 = document.getElementsByClassName("list-item");
// console.log(listItems2);

// const list3 = document.querySelector(".list");
// console.log(list3);
// const list4 = document.querySelector("body > ul#list.list");
// console.log(list4);

// const listItems3 = document.querySelectorAll(".list-item");
// console.log(listItems3);

// const listItem1 = document.querySelector(".list-item");
// console.log(listItem1);

// const listItemsArray = Array.from(listItems3);
// console.log(listItemsArray);

// const firstListItem = listItemsArray[0];
// console.log(firstListItem);
// console.log(firstListItem.innerHTML);
// console.log(firstListItem.innerText);
// console.log(firstListItem.textContent);

// firstListItem.innerText += "<span>span here</span>";
// firstListItem.innerHTML += "<span>span here</span>";
// firstListItem.textContent += "<span>span here</span>";

// console.log(firstListItem.style);
// firstListItem.style.fontSize = "30px";
// firstListItem.style.color = "red";
// console.log(firstListItem.id);

// firstListItem.remove();

// console.log(firstListItem);

// list3.innerHTML += firstListItem.outerHTML;

// TASK 1

// const ul = document.getElementById("list");
// console.log(ul);
// const list = document.getElementsByClassName("list-item");
// console.log(list);
// const listItem1 = document.getElementsByTagName("li");
// console.log(listItem1);

// const selector = ".list-item:nth-child(3)";
// const listItem2 = document.querySelector(selector);
// console.log(listItem2);

// const listItems3 = document.querySelectorAll("li");
// console.log(listItems3);

// TASK 2
// const remove = document.querySelector(".remove");
// remove.remove();
// console.log(remove);

// const bigger = document.querySelector(".bigger");
// bigger.classList.replace("bigger", "active");
// console.log(bigger.classList);
// console.log(bigger.className);

// TASK 3

// let liElements = Array.from(document.querySelectorAll(".store li"));

// liElements.forEach((element) => {
//    if (+element.textContent.split(":")[1] === 0) {
//       element.textContent = element.textContent.replace("0", "закончился");
//       element.style.color = "red";
//       element.style.fontWeight = "600";
//    }
// });

// TASK 4
const targetElement =
   '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';

let liElements = Array.from(document.querySelectorAll(".list-item"));
console.log(liElements);

const fifthItem = liElements.find((el) => el.textContent === "Item 5");
fifthItem.innerHTML = targetElement;
