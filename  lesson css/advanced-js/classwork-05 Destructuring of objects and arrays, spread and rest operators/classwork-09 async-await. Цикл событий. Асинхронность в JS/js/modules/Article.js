import Element from "./Element.js";
export default class Article {
    constructor({ title, intro, text, date, author }) {
        this.title = title;
        this.intro = intro;
        this.text = text;
        this.date = date;
        this.author = author;
    }

    render() {
        const title = new Element("h3", this.title).createElement();
        const introText = new Element("p", this.intro).createElement();
        const text = new Element("p", this.text).createElement();
        const date = new Element("p", this.date).createElement();
        const author = new Element("p", this.author).createElement();

        const fragment = document.createDocumentFragment();
        fragment.append(title, introText, text, date, author);
        const li = new Element("li", fragment).createElement();

        return li;
    }
}
