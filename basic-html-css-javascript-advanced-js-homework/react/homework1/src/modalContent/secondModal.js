const secondModal = {
    secondModalTitle: 'Что-то там добавить?',
    secondModalText: `Посоле сохранения данные невозможно редактировать`,
    textBtnSecond: 'Open second modal',
    colorBtnSecond: '#03fc52'
}
export default secondModal