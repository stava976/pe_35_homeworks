import { USER_LOGGED_IN, USER_LOGGED_OUT } from "./user.actions";

const initialState = {
  isLoggedIn: false,
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: true,
      }

    case USER_LOGGED_OUT:
      return {
        ...state,
        isLoggedIn: false,
      }

    default:
      return state;
  }
}

export default userReducer;