"use strict";
let contentImg = document.querySelectorAll('.image-to-show');
let currentImg = 0;
let cycledSlider = setInterval(imgSlider, 3000);
function imgSlider() {
    contentImg[currentImg].classList.remove('current-img');
    currentImg = (currentImg + 1) % contentImg.length;
    contentImg[currentImg].classList.add('current-img');
}
const btn = document.createElement('div');
btn.classList.add('btn-new');
document.querySelector("script").before(btn);

const TimeOut = document.createElement('button');
TimeOut.classList.add('btn');
TimeOut.innerText = 'Прекратить';
btn.appendChild(TimeOut);
TimeOut.addEventListener('click', () => {
    clearInterval(cycledSlider);
});
const resumeTimeOut = document.createElement('button');
resumeTimeOut.classList.add('btn');
resumeTimeOut.innerText = 'Возобновить показ';
btn.appendChild(resumeTimeOut);
document.querySelector('script').before(btn);
resumeTimeOut.addEventListener('click', () => {
    cycledSlider = setInterval(imgSlider, 3000)
});


