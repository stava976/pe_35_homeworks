"use strict";

// function User(name, age) {
//     this.name = name;
//     this.age = age;
// }

// const user = new User("Uasya", 18);
// // console.log(user);

// // console.log(user.__proto__);

// // console.log(User.prototype);

// // console.log(user.__proto__ === User.prototype);

// function showName() {
//     return this.name;
// }

// User.prototype.showName = showName;

// console.log(user.showName());
// console.log(user);

// const obj = new Object();
// console.log(typeof obj);
// console.log(obj instanceof Object);
// console.log(null instanceof Object);
// console.log(user instanceof User);

// function foo() {}
// console.log(foo);
// let bar = { a: "a4" };

// foo.prototype = bar;
// // Object {a: "a4"}
// const baz = Object.create(bar);
// // Object {a: "a4"}
// console.log(baz instanceof foo);
// // true. oops.
// console.log(bar instanceof foo);
// console.log(baz.__proto__ === foo.prototype);
// // true. oops.

// console.log(baz.__proto__);
// console.log(foo.prototype);
// console.log(bar);

// bar.count = 44;
// console.log(baz.__proto__);
// console.log(foo.prototype);
// console.log(typeof foo);
//
// class User {
//     constructor(name = "", age = 0) {
//         this.age = age;
//         this.name = name;
//     }
//     showName() {
//         return this.name;
//     }
//     checkStatus() {
//         return this.status || "guest";
//     }
// }
//
// const user = new User();
// // console.log(User);
// // console.log(user.checkStatus());
// console.log(user);
//
// class UserAdmin extends User {
//     constructor(name = "Uasya", age = 18, status = "") {
//         super(name, age);
//         this.status = status;
//         this.superPower = function () {
//             return "I'm Allmighty";
//         };
//     }
//     userHandler() {
//         return "Carry by admin";
//     }
// }
//
// const admin = new UserAdmin("Alex", 16, "Admin");
// console.log(admin);
// console.log(admin.checkStatus());
// console.log(admin.showName());
//
// class SubAdmin extends UserAdmin {}
// const subAdmin = new SubAdmin();
//
// console.log(subAdmin);
// console.log(subAdmin.checkStatus());
// console.log(subAdmin.showName());
//task 1
class Patient {
    constructor(name, date, gender) {
        this.name = name;
        this.date = date;
        this.gender = gender;
    }
}
class PAtientCardio extends Patient{
    constructor(name, date, gender, presure, problems) {
        super();
        this.presure = presure;
       this.problems = problems;
    }
}
class PAtientDantist extends Patient{
    constructor(name, date, gender, dataVisit, helth) {
        super();
        this.dataVisit = dataVisit;
        this.helth = helth;
    }
}
const patientCardio = PAtientCardio("sssas","12.12.2021", "men", "sdsd", "sdsf"
);