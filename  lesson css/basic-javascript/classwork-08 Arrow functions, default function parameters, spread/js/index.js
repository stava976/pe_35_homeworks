"use strict";

// function summ(a = 0, b = 0) {
//    return a + b;
// }

// const summ = (a, b) => {
//    // to do
//    return a + b;
// };
// const summ = (a, b) => a + b;

// const getObj = () => ({ name: "Uasya" });

// const user = {
//    name: "Uasya",
//    getName: () => {
//       return this.name;
//    },
// };

// console.log(user.getName());

// const summ = (a = 0, b = 0) => a + b;
// console.log(summ(3));

// function summ() {
//    let res = 0;
//    for (const value of arguments) {
//       res += value;
//    }
//    return res;
// }
// console.log(summ(1, 2, 3, 4, 5, 6));

/////////////////////

// const summAllNums = (...params) => {
//    console.log(params);
//    console.log(...params);
// };
// console.log(summAllNums(1, 2, 3, 4, 5, 6));

// const user = {
//    name: "Uasya",
// };

// const user2 = { ...user, age: 18 };
// // console.log(user2);

// const innerArr = ["a", "b", "c"];
// console.log(innerArr);
// console.log([1, 2, 3, 4, 5, innerArr]);
// console.log([1, 2, 3, ...innerArr, 4, 5]);

// TASK 1
// const summ = (a, b) => a + b;
// console.log(summ(2, 5));

// const summ = (a, b) => {
//    return a + b;
// };
// console.log(summ(2, 9));

// TASK 2
// const sum = (...params) => {
//    let answ = 0;
//    if (params.length < 2) {
//       return "error";
//    } else {
//       for (let i = 0; i < params.length; i++) {
//          if (isNaN(params[i]) || typeof params[i] !== "number") {
//             return `ooops nubmer ${i}`;
//          }
//       }
//       for (let j = 0; j < params.length; j++) {
//          answ += params[j];
//       }
//       return answ;
//    }
// };
// console.log(sum(1, 2, "2"));

// TASK 3
// const getMaxNumber = (...params) => {
//    if (params.length < 2) {
//       return "error";
//    }

//    for (let i = 0; i < params.length; i++) {
//       if (isNaN(params[i]) || typeof params[i] !== "number") {
//          return `ooops nubmer ${i}`;
//       }
//    }

//    return Math.max(...params);
// };
// console.log(getMaxNumber(1, 2, 3, 6, 9));

// TASK 4
// const log = (message = "«Внимание! Сообщение не указано.»", repeat = 1) => {
//    if (
//       typeof message !== "string" ||
//       isNaN(repeat) ||
//       typeof repeat !== "number"
//    ) {
//       console.log(`error`);
//       return;
//    }
//    for (let index = 0; index < repeat; index++) {
//       console.log(message);
//    }
// };
// log("lol", 5);

// TASK 5

const add = (a, b) => a + b;
const subtract = (a, b) => a - b;
const multiply = (a, b) => a * b;
const divide = (a, b) => a / b;

const calc = (a, b, operation) => {
   return operation(a, b); // operation - callback function
};

console.log(calc(2, 6, multiply));
console.log(calc(2, 6, add));
console.log(calc(2, 6, divide));

[4, 6, 1, 8, 3, 8].sort((a, b) => a - b);
console.log([4, 6, 1, 8, 3, 8].sort((a, b) => a - b));
