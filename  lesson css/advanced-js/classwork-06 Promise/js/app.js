"use strict";

// const posts = [
//     { title: "First post", body: "This is first post" },
//     { title: "Second post", body: "This is second post" },
// ];
// getPosts();

// function getPosts() {
//     setTimeout(() => {
//         posts.forEach((post, index) => {
//             console.log(index + 1, post.title);
//             console.log(post.body);
//         });
//     }, 1000);
// }

// function createPost(post, callBack) {
//     setTimeout(() => {
//         posts.push(post);

//         callBack();
//     }, 2000);
// }

// createPost({ title: "Third post", body: "This is third post" }, getPosts);

// createPost({ title: "Third post", body: "This is third post" }, function () {});

// getPosts();

// PROMISE
// const posts = [
//     { title: "First post", body: "This is first post" },
//     { title: "Second post", body: "This is second post" },
// ];

// const promise = new Promise(function (resolve, reject) {
//     try {
//         throw new Error("Our error");
//         setTimeout(() => {
//             posts.push({ title: "Third post", body: "This is third post" });

//             resolve(posts);
//         }, 2000);
//     } catch (e) {
//         //   reject(new Error(e.message));
//         reject(e.message);
//     }
// });
// console.log(promise);
// promise
//     .then((data) => {
//         data.forEach((post, index) => {
//             console.log(index + 1, post.title);
//             console.log(post.body);
//         });
//     })
//     .catch((e) => {
//         //   console.log(e.message);
//         console.log(e);
//     })
//     .finally(() => {
//         console.log("The end");
//     });

// var p1 = Promise.resolve(3);
// var p2 = 1337;
// var p3 = new Promise((resolve, reject) => {
//     setTimeout(resolve, 100, "foo");
// });
// Promise.all([p1, p2, p3]).then((values) => {
//     console.log(values);
// });

// const promise4 = new Promise(function (resolve, reject) {
//     setTimeout(() => {
//         resolve(4);
//     }, 2000);
// });
// Good
// promise4
//     .then((data) => {
//         return new Promise(function (resolve) {
//             resolve(data);
//         });
//     })
//     .then((data) => {
//         console.log(data);
//     });
// Not good
// promise4.then((data) => {
//     const promise5 = new Promise(function (resolve) {
//         resolve(data);
//     });
//     promise5.then((data) => {
//         console.log(data);
//     });
// });

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
// const ENTITY = ["vehicles"];

// const getVehicles = new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest();
//     xhr.open("GET", BASE_URL + ENTITY[0] + "/4");
//     xhr.send();

//     xhr.onload = () => resolve(xhr.response);
//     xhr.onerror = () => reject(e);
// });

// getVehicles
//     .then((response) => {
//         response = JSON.parse(response);
//         console.log(response);
//         console.log(response.consumables);
//         //   console.log(response[0]);
//     })
//     .catch((error) => {
//         console.log(error);
//     });

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
// const ENTITY = ["vehicles"];
// const root = document.querySelector("#root");
// class Server {
//     constructor(url) {
//         this.url = url;
//     }
//     getData() {
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");
//          //  let li = document.createElement("li");
//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);
//                 for (let i = 0; i < response.length; i++) {
//                     let li = document.createElement("li");
//                     li.textContent = `${response[i].name}`;
//                     ul.append(li);
//                 }
//                 //   console.log(response[0]);
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }
// const url = `https://ajax.test-danit.com/api/swapi/films/`;
// const getFilms = new Server(url);
// getFilms.getData();

// class Server {
//     constructor(url) {
//         this.url = url;
//     }

//     getData() {
//         const ENTITY = ["planets"];
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url + ENTITY[0]);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");

//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);

//  let newarr = response.sort(function (a, b) {
//      if (b.diameter > a.diameter) {
//          return 1;
//      }
//  });
//  console.log(newarr);

//  for (let i = 0; i < response.length; i++) {
//      let li = document.createElement("li");
//      li.textContent = `${response[i].name} - ${response[i].diameter}`;
//      ul.append(li);
//  }
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }

// const planets = new Server("https://ajax.test-danit.com/api/swapi/");
// planets.getData();

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
// const ENTITY = ["vehicles"];
// const root = document.querySelector("#root");
// class Server {
//     constructor(url) {
//         this.url = url;
//     }
//     getData() {
//         const getVehicles = new Promise((resolve, reject) => {
//             const xhr = new XMLHttpRequest();
//             xhr.open("GET", this.url);
//             xhr.send();
//             xhr.onload = () => resolve(xhr.response);
//             xhr.onerror = () => reject(e);
//         });
//         let ul = document.createElement("ul");
//          //  let li = document.createElement("li");
//         getVehicles
//             .then((response) => {
//                 response = JSON.parse(response);
//                 console.log(response);
//                 for (let i = 0; i < response.length; i++) {
//                     let li = document.createElement("li");
//                     li.textContent = `${response[i].name}`;
//                     ul.append(li);
//                 }
//                 //   console.log(response[0]);
//             })
//             .catch((error) => {
//                 console.log(error);
//             });
//         root.append(ul);
//     }
// }
// const url = `https://ajax.test-danit.com/api/swapi/films/`;
// const getFilms = new Server(url);
// getFilms.getData();

// task 3
const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
const ENTITY = ["vehicles"];
const root = document.querySelector("#root");
class Server {
    constructor(url) {
        this.url = url;
    }
    getReqest(url) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.send();
            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(e);
        });
    }
    createList(arrContent) {
        const ul = document.createElement("ul");
        const items = arrContent.map((el) => {
            let li = document.createElement("li");
            li.textContent = `${el}`;
            return li;
        });

        ul.append(...items);
        return ul;
    }
    getData() {
        this.getReqest(this.url)
            .then((response) => {
                response = JSON.parse(response);
                const filmNames = response.map((el) => el.name);
                const listFilms = this.createList(filmNames);

                response.forEach((el, index) => {
                    const planetsReq = el.planets.map((pl) => {
                        return this.getReqest(pl);
                    });
                    Promise.all(planetsReq).then((values) => {
                        values = values.map((el) => {
                            return JSON.parse(el).name;
                        });
                        const listPlanets = this.createList(values);
                        listFilms.children[index].append(listPlanets);
                    });
                });

                root.append(listFilms);
            })
            .catch((error) => {
                console.log(error);
            });
    }
}
const url = `https://ajax.test-danit.com/api/swapi/films/`;
const getFilms = new Server(url);
getFilms.getData();
