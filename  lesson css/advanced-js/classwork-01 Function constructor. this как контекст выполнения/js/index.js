"use strict";

// const obj = {};

// const name = "Uasya";
// const age = 33;
// Object.create();

// function objectCreate(...params) {
//     return { ...params };
// }

// console.log(objectCreate(name, age));

// function ObjectCreate(propName = "Empty", propAge = 18) {
//     this.name = propName;
//     this.age = propAge;

//     this.showName = function () {
//         console.log(this.name);
//     };
// let newAge = 33;
// const newName = "Petya";
// return {
//     is: false,
// };
// }
// const user = new ObjectCreate("Uasya", 33);
// const obj2 = new ObjectCreate("Uasya2");

// const user1 = {
//     name: "Petya",
// };
// for (const key in user) {
//     if (Object.hasOwnProperty.call(user, key)) {
//         const element = user[key];
//         console.log(element);
//     }
// }

// console.log("12".hasOwnProperty("2"));
// console.log(user);
// bind, call, apply
// user1.showName = user.showName.bind(user1);
// user1.showName();

// user.showName.bind(user1)();
// user.showName();

// user.showName.call(user1);
// user.showName.apply(user1);

// console.log(obj2);
// user.showName();

// const arr = new Array(1, 2, 3, 4, 5);
// console.log(arr);
// console.log(typeof arr);

// arr.forEach((el) => {
//     console.log(el);
// });

// const str = new String(123);
// console.log(str);
// console.log(str.split());
// console.log(typeof str);
// console.log("123".split());

// TASK 1
// const questionText = "Девиз дома Баратеонов";
// const questionAnswer = "Нам ярость!";

// function Question(paramQuestion, paramAnswer) {
//     this.question = paramQuestion;
//     this.answer = paramAnswer;

//     this.render = function (rootElem = document.querySelector("#root")) {
//         let token = 0;

//         let link = document.createElement("a");
//         link.textContent = this.question;
//         link.style.display = "block";
//         link.classList.add("question");
//         let p = document.createElement("p");
//         p.textContent = this.answer;
//         rootElem.append(link);

//         link.addEventListener("click", () => {
//             if (!token) {
//                 link.after(p);
//                 token = 1;
//                 console.log(link);
//             } else {
//                 p.remove();
//                 token = 0;
//             }
//         });
//     };
// }

// const question1 = new Question(questionText, questionAnswer);
// question1.render();

// TASK 2
function Stopwatch(container) {
    this._time = 0;
    this._timer = null;
    this.container = container;

    this.start = function () {
        let start = 0;
        // const that = this;
        this._timer = setInterval(
            function () {
                // that.setTime(start++);
                this.setTime(start++);
                this.container.textContent = this.getTime();
            }.bind(this),
            100
        );
    };
    this.stop = function () {
        clearInterval(this._timer);
    };
    this.reset = function name() {
        this.setTime(0);
        this.container.textContent = this.getTime();
    };
    this.setTime = function name(newTime) {
        if (newTime >= 0 && newTime % 1 === 0) {
            this._time = newTime;
            return {
                status: "success",
            };
        } else {
            return {
                status: "error",
                message: "argument must be positive integer",
            };
        }
    };
    this.getTime = () => this._time;
}
const startBtn = document.getElementById("start-time");
const stopBtn = document.getElementById("stop-time");
const resetBtn = document.getElementById("reset-time");

const stopWatchContainer = document.getElementById("time");
const stopwatch = new Stopwatch(stopWatchContainer);

startBtn.addEventListener("click", stopwatch.start.bind(stopwatch));
stopBtn.addEventListener("click", stopwatch.stop.bind(stopwatch));
resetBtn.addEventListener("click", stopwatch.reset.bind(stopwatch));
