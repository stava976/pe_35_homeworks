import React from 'react';
import { Formik, Field, Form, useField, useFormikContext } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { addTeam } from '../../redux/teams/teams.actions';
import * as Yup from 'yup';
import { useEffect } from 'react';

const API_SERVER = 'http://localhost:3001';

const CreateTeamSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  city: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  coach: Yup.string().required('Required'),
});

const TeamField = (props) => {
  const [field, meta] = useField(props)

  const {
    values: { city },
    touched,
    setFieldValue,
  } = useFormikContext();

  useEffect(() => {
    if (!meta.touched) {
      setFieldValue(props.name, city);
    }
  }, [city])

  console.log(city);

  return (
    <input {...props} {...field} />
  )
}

const validateCoach = (coachId) => {
  return fetch(`${API_SERVER}/coach/${coachId}`, {
    method: 'GET',
  })
    .then((response) => response.json())
    .then(coach => {
      if (coach.team) {
        return 'This coach is touched...'
      } else {
        return true
      }
    })
}

const CreateTeam = () => {
  const coaches = useSelector(state => state.coaches.coachesList)

  const dispatch = useDispatch();

  return (
    <div>
      <h1>Create team</h1>
      <Formik
        validateOnChange={false}
        validationSchema={CreateTeamSchema}
        initialValues={{
          name: '',
          city: '',
          coach: '',
        }}
        onSubmit={(values) => {
          dispatch(addTeam(values));;
        }}
      >
        {({ errors, touched }) => {
          console.log(touched);
          console.log(errors);
          return (
            <Form>
              <label style={{ position: 'relative' }}>Name:
              <TeamField id="name" name="name" />
                {errors.name && touched.name ? (
                  <span style={{ color: 'red', position: 'absolute', top: '100%', left: '0' }}>{errors.name}</span>
                ) : null}
              </label>


              <label style={{ position: 'relative' }}>City:
              <Field id="city" name="city" />
                {errors.city && touched.city ? (
                  <span style={{ color: 'red', position: 'absolute', top: '100%', left: '0' }}>{errors.city}</span>
                ) : null}
              </label>

              <label style={{ position: 'relative' }}>Coach:
              <Field
                  validate={validateCoach}
                  as='select'
                  id="coach"
                  name="coach"
                >
                  {
                    coaches.map(coach => {
                      return <option key={coach.id} value={coach.id}>{coach.name}</option>
                    })
                  }
                </Field>
                {errors.coach && touched.coach ? (
                  <span style={{ color: 'red', position: 'absolute', top: '100%', left: '0' }}>{errors.coach}</span>
                ) : null}
              </label>

              <button type="submit">Submit</button>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}

export default CreateTeam




// const MyField = (props) => {
//   const {
//     values: { textA, textB },
//     touched,
//     setFieldValue,
//   } = useFormikContext();
//   const [field, meta] = useField(props);

//   React.useEffect(() => {
//     // set the value of textC, based on textA and textB
//     if (
//       textA.trim() !== '' &&
//       textB.trim() !== '' &&
//       touched.textA &&
//       touched.textB
//     ) {
//       setFieldValue(props.name, `textA: ${textA}, textB: ${textB}`);
//     }
//   }, [textB, textA, touched.textA, touched.textB, setFieldValue, props.name]);

//   return (
//     <>
//       <input {...props} {...field} />
//       {!!meta.touched && !!meta.error && <div>{meta.error}</div>}
//     </>
//   );
// };

// function App() {
//   // Note that we provide initalValues all 3 fields.
//   const initialValues = { textA: '', textB: '', textC: '' };
//   return (
//     <div className="App">
//       <Formik
//         initialValues={initialValues}
//         onSubmit={async (values) => alert(JSON.stringify(values, null, 2))}
//       >
//         <div className="section">
//           <h1>Dependent Formik Field Example</h1>
//           <p style={{ color: '#555' }}>
//             This is an example of how to set the value of one field based on the
//             current values of other fields in Formik v2. In form below, textC's
//             value is set based on the current values of fields textA and textB.
//           </p>
//           <div>
//             <small>
//               <em>
//                 Instructions: enter values for textA, and textB, and then watch
//                 textC.
//               </em>
//             </small>
//           </div>
//           <Form>
//             <label>
//               textA
//               <Field name="textA" />
//             </label>
//             <label>
//               textB
//               <Field name="textB" />
//             </label>
//             <label>
//               textC
//               <MyField name="textC" />
//             </label>
//             <button type="submit">Submit</button>
//           </Form>
//         </div>
//       </Formik>
//       <div style={{ marginTop: 16 }}>
//         Notice the following:
//         <ul>
//           <li>
//             textC's value is set after fields textA and textB have been touched
//             and if they are not empty.
//           </li>
//           <li>
//             textC is <i>still</i> editable after being set programmatically.
//             However, the value will be overwritten if/whenever there are (new)
//             changes to textA and textB. This is because of the dependency array
//             in our custom field's React.useEffect; it only runs when textA or
//             textB change or have been touched. Since Formik's `setFieldValue` is
//             equivalent between renders and so is the field's name prop, this
//             works as expected. You could alter this overrwriting behavior by
//             keeping track of whether textC was been updated by field.onChange or
//             useEffect. This might be very useful if you want to alert the end
//             user that their changes to textC will be lost.
//           </li>
//         </ul>
//       </div>
//     </div>
//   );
// }

// const rootElement = document.getElementById('root');
// ReactDOM.render(<App />, rootElement);
