"use strict";
function getFilms(){
    let data = fetch('https://swapi.dev/api/films/')
        .then(response => response.json())
        .then(data =>  data.results)
    return data
}
function getCharacters(){
    let films = getFilms()
    films.then(data => data.forEach(film => {
        addPage(film)
        film.characters.forEach(character => {
            fetch(character)
                .then(response => response.json())
                .then(data => {
                    const dfilm = document.querySelector(`.id${film.episode_id}`)
                    dfilm.innerHTML += `<p>${data.name}</p>`
                })
        })
    }))
}
function addPage(film){
    document.write(`<div>
        <p>${film.episode_id}</p>
        <h2>${film.title}</h2>
        <p>${film.opening_crawl}</p>
        <div class=id${film.episode_id}></div>
    </div>`)
}
getCharacters()
console.log(getFilms());



