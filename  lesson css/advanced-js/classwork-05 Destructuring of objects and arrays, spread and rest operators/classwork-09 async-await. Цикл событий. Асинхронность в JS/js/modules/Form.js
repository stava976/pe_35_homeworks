export default class Form {
    constructor(formTemplate, formHandler) {
        this.formTemplate = formTemplate;
        this.formHandler = formHandler;
    }
    render() {
        const fTpl = this.formTemplate.content;
        const form = fTpl.querySelector("form").cloneNode(false);
        console.log(form);
        const title = fTpl.querySelector("#title").cloneNode(false);
        const intro = fTpl.querySelector("#intro").cloneNode(false);
        const text = fTpl.querySelector("#text").cloneNode(false);
        const date = fTpl.querySelector("#date").cloneNode(false);
        const author = fTpl.querySelector("#author").cloneNode(false);
        const submit = fTpl.querySelector('[type = "submit"]').cloneNode(false);

        form.append(title, intro, text, date, author, submit);
        form.addEventListener("submit", this.formHandler);
        return form;
    }
}
