"use strict";
class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get nameInfo(){
        return this.name;
    }
    get ageInfo(){
        return this.age;
    }
    get salaryInfo(){
        return this.salary;
    }
    set nameInfo(newName){
        this.name = newName;
    }

    set ageInfo(newAge){
        this.age = newAge;
    }
    set salaryInfo(newSalary){
        this.salary = newSalary;
    }
}
class Programmer extends Employee{
    constructor(name, age, salary, lang ){
        super(name, age, salary);
        this.lang = lang;
    }
    get salaryInfo(){
        return this.salary * 3;
    }
}
const employee = new Employee('Valera', 33, 35000);
console.log({employee});
const programmer = new Programmer('Sasha',37,39000,['php','js', 'react']);
console.log({programmer});
console.log(programmer.salaryInfo);
;
