export const loggerMiddleware = store => next => action => {
  next(action);
  console.log(
    `Событие: ${action.type}. 
     Payload: ${JSON.stringify(action.payload)}`
  );
}
