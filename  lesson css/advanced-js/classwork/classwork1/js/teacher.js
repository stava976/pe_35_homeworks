class Teacher extends Entity {
  constructor(firstName, lastName, id) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.element = null;
    this.id = id;
    this.listElement = document.querySelector(".js-teacher-list");

    this.callbacks = {
      'delete': [],
    };    
  }

  addEventListener(eventName, callback) {
    this.callbacks[eventName].push(callback);
  }

  async remove() {
    this.callbacks.delete.forEach((callback) => {
      callback();
    });
  }
}


