import React from 'react'
import Border from './Border'

class User extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      blockToShow: 'none',
    }
  }

  showAge = () => {
    this.setState({
      blockToShow: 'age',
    })
  }
  
  showGender = () => {
    this.setState({
      blockToShow: 'gender',
    });
  }

  hideAll = () => {
    this.setState({
      blockToShow: 'none',
    });
  }

  render() {
    return (
      <>
        <h3>Bilbo</h3>
        <h3>Baggins</h3>
        <button onClick={this.hideAll}>Hide All</button>
        <button onClick={this.showAge}>Show Age</button>
        <button onClick={this.showGender}>Show Gender</button> <br />
        
        {this.state.blockToShow === 'age' && <Border>36</Border>}
        {this.state.blockToShow === 'gender' && <Border><h2>male</h2></Border>}


        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Id quos laboriosam qui porro expedita pariatur eos, excepturi officia sint, quasi dignissimos nisi asperiores. Cum provident cupiditate amet ex! Perspiciatis, placeat.
      </>
    )
  }
}

export default User;