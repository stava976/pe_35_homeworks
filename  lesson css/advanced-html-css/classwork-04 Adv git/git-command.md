## Create branch

### var 1

git checkout -b branchName

### var 2

git branch branchName

git checkout branchName

## Check all local branch

git branch

## Check all remote branch

git branch -r

## Switch to another branch

git checkout branchName

## Merge two branches

git checkout targetBranchName

git merge sourceBranchName

# Полезные материалы

https://git-scm.com/book/ru/v2/%D0%92%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-Git-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-%D0%B2%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B8-%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D1%8F
