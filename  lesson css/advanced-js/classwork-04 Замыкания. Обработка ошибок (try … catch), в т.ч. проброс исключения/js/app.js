// let a = 3;

// function fn() {
//     return a + 3;
// }

// function fnWrapper() {
//     let a = 3;

//     return function fnInner() {
//         return a + 3;
//     };
// }

// function summ(a, b, c) {
//     return a + b + c;
// }
// summ(a, b, c)

// function summ(a) {
//     return function (b) {
//         return function (c) {
//             return a + b + c;
//         };
//     };
// }
// summ(a)(b)(c);

// const sumTwoAndTwoAnd = summ(2)(2);

// const baadJson = "{bad json}";

// function parseJson(json) {
//     try {
//         return JSON.parse(json);
//     } catch (e) {
//         console.dir(e.message);
//     } finally {
//         console.log("Working always");
//     }
// }

// console.log(parseJson(baadJson));
// console.log(parseJson(JSON.stringify({ a: 3 })));
// console.log("hello");

// function toUpp(str) {
//     try {
//         return str.toUpperCase();
//     } catch (e) {
//         console.dir(e);
//         console.dir(e.name);
//         throw new Error(e.message);
//     }
// }

// let str = "fghjkl";
// let upperStr;

// try {
//     upperStr = toUpp(null);
// } catch (e) {
//     console.log(e.message);
//     upperStr = str;
// }

// console.log(upperStr);

// TASK 1
// class Change {
//     constructor(dom, sizePlus, sizeMinus) {
//         this.dom = dom;
//         this.sizePlus = sizePlus;
//         this.sizeMinus = sizeMinus;
//     }

//     plusSize() {
//         let sizeText = parseInt(this.dom.style.fontSize);
//         sizeText += this.sizePlus;
//         this.dom.style.fontSize = `${sizeText}px`;
//         console.log(sizeText);
//     }
//     minusSize() {
//         let sizeText = parseInt(this.dom.style.fontSize);
//         sizeText -= this.sizeMinus;
//         this.dom.style.fontSize = `${sizeText}px`;
//         console.log(sizeText);
//     }
// }

// const p = document.querySelector("#text");
// const change = new Change(p, 2, 3);

// const btnSizePlus = document.querySelector("#increase-text");
// btnSizePlus.addEventListener("click", change.plusSize.bind(change));
// const btnSizeMinus = document.querySelector("#decrease-text");
// btnSizeMinus.addEventListener("click", change.minusSize.bind(change));

// TASK 2

// class createProduct {
//     constructor(name, fullName, article, price) {
//         this.name = name;
//         this.fullName = fullName;
//         this.article = article;

//         Object.defineProperty(this, "price", {
//             get: function () {
//                 return price;
//             },
//             set: function (newPrice) {
//                 if (newPrice > 0 && newPrice % 1 === 0) {
//                     price = newPrice;
//                 }
//             },
//         });
//     }
// }

// const notebook = new createProduct(
//     "lenovo X120S",
//     "lenovo X120S(432-44) W",
//     3332,
//     23244
// );
// console.log(notebook.price); // выведет 23244
// notebook.price = -4; // присвоение не произойдет
// console.log(notebook.price); //выведет 23244
// notebook.price = 22000;
// console.log(notebook.price); // выведет 22000

// TASK 3
class Slider {
    constructor(effect, speed, autostart, slides) {
        this.effect = effect;
        this.speed = speed;
        this.autostart = autostart;
        this.slides = [...slides];
    }

    addSlide(slide) {
        if (
            typeof slide === "object" &&
            slide !== null &&
            !Array.isArray(slide)
        ) {
            this.slides.push(slide);
        } else {
            console.log("error");
        }
    }

    // function toUpp(str) {
    //     try {
    //         return str.toUpperCase();
    //     } catch (e) {
    //         console.dir(e);
    //         console.dir(e.name);
    //         throw new Error(e.message);
    //     }
    // }

    getSlide(order) {
        try {
            if (this.slides[order - 1]) {
                return this.slides[order - 1];
            }
            return new Error("No such slider");
        } catch (e) {
            // throw new Error(e.message);
        }
    }
    deleteSlide(number) {
        if (number < this.slides.length) {
            console.log(this.slides.length);
            this.slides.splice(number - 1, 1);
        }
    }
}

try {
    const slides = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }];

    const slider = new Slider("fade", 100, true, slides);
    slider.addSlide({ id: 6 });
    slider.deleteSlide(3);
    slider.deleteSlide(6);

    slides.push("ee");
    console.log(slider.slides);

    console.log(slider.getSlide(0));
    console.log(slider.getSlide(4));
} catch (e) {
    console.log(e.message);
}
