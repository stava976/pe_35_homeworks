class Auth {
  constructor() {
    const loginButton = document.querySelector('.js-login-button');

    loginButton.addEventListener('click', this.login.bind(this));
  }
  login(email, password) {
    mediator.publishEvent('login');
    return Promise.resolve();
  }
}
