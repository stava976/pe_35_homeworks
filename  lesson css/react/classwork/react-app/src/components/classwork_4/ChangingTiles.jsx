import React from 'react';
import "./Tiles.css"

const generateInitialTiles = () => {
  const rowsArr = Array.from(Array(25))
  return rowsArr.map(() => generateRow())
}

const generateRow = () => {
  return Array.from(Array(25))
    .map(() => Math.random() < 0.5 ? 'black' : 'white')
}

const getElementByIndex = (arr, row, col) => {
  if (arr[row] === undefined) {
    return undefined
  } else {
    return arr[row][col]
  }
}

class ChangingTiles extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tiles: generateInitialTiles(),
    }
  }

  changeColor = (e) => {
    const updatedTiles = this.state.tiles.map((row, rowNumber) => {
      return row.map((color, columnNumber) => {
        const neighbours = [
          getElementByIndex(this.state.tiles, rowNumber -1, columnNumber - 1),
          getElementByIndex(this.state.tiles, rowNumber - 1, columnNumber),
          getElementByIndex(this.state.tiles, rowNumber - 1, +columnNumber + 1),
          getElementByIndex(this.state.tiles, rowNumber, columnNumber - 1),
          getElementByIndex(this.state.tiles, rowNumber, +columnNumber + 1),
          getElementByIndex(this.state.tiles, +rowNumber + 1, columnNumber - 1),
          getElementByIndex(this.state.tiles, +rowNumber + 1, columnNumber),
          getElementByIndex(this.state.tiles, +rowNumber + 1, +columnNumber + 1),
        ]
        let black = 0;
        let white = 0;

        neighbours.forEach(neighbour => {
          if (neighbour === 'black') {
            black++;
          } else if (neighbour === 'white') {
            white++;
          }
        })

        if (black > white) {
          return 'black'
        } else if (white > black) {
          return 'white'
        }

        return color
      })
    })
    
    this.setState({
      tiles: updatedTiles,
    })
  }

  render() {
    console.log(this.state);
    return (
      <div onClick={this.changeColor} className="tiles-wrapper">
        {this.state.tiles.map((row, rowNumber) => {
          const rowElements = row.map((tileColor, columnNumber) => {
            return <div 
              data-row={rowNumber}
              data-column={columnNumber}
              key={`${rowNumber}${columnNumber}`} 
              style={{ backgroundColor: tileColor}} 
              className="tile"
            ></div>
          })
          return <React.Fragment key={rowNumber}>{rowElements}<br/></React.Fragment>
        })}
      </div>
    );
  }
}

export default ChangingTiles;