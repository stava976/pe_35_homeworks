import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Counter from './Counter';
import counterReducer from './redux/counter.reducer';


const store = createStore(counterReducer);

const CounterApp = () => {
  return (
    <Provider store={store}>
      <Counter />
    </Provider>
  );
}

export default CounterApp;
