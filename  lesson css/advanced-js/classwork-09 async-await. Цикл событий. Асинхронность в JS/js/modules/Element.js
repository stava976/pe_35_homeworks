export default class Element {
    constructor(tagName, content, id, classes) {
        this.tagName = tagName;
        this.id = id;
        this.classes = classes;
        this.content = content;
    }
    createElement() {
        let element = document.createElement(this.tagName);
        if (this.classes) {
            element.className = this.classes.join(" ");
        }
        if (this.id) {
            element.id = this.id;
        }

        if (typeof this.content !== "object") {
            element.innerHTML = this.content;
        } else {
            element.append(this.content);
        }

        return element;
    }
}
