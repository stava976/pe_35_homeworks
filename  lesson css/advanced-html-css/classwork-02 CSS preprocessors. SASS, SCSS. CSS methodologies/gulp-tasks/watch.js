const { watch, parallel } = require("gulp");
const { bs } = require("./serv.js");
const { styles } = require("./styles.js");
const { scripts } = require("./scripts.js");

const watcherFn = () => {
   watch("./index.html").on("change", bs.reload);
   watch("./src/js/*.js").on("change", parallel(scripts));
   watch("./src/scss/**/*.scss").on("change", parallel(styles));
};

exports.watcher = watcherFn;
