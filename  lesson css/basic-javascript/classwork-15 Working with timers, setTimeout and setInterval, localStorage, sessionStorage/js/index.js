// const timer = setTimeout(timerCallBack, 1500);
// const startTime = new Date();

// function timerCallBack() {
//    const finishTime = new Date();
//    console.log("timer alarm!");
//    console.log(finishTime - startTime);
//    clearTimeout(timer);
// }

// const infiniteTimer = setInterval(infiniteTimerCallBack, 1500);

// function infiniteTimerCallBack() {
//    console.log("infinite timer alarm!");
//    clearInterval(infiniteTimer);
// }

// const age = 18;

// const user = {
//    age: 18,
//    name: "Uasya",
// };

// JavaScript Object Notation -> JSON
// const userJSON = JSON.stringify(user);
// console.log(userJSON);

// localStorage.setItem("user", userJSON);

// const userFromStorage = localStorage.getItem("user");
// console.log(userFromStorage);

// const newUser = JSON.parse(userFromStorage);
// console.log(newUser);

// console.log(`${user}`);

// localStorage.setItem("age", age);
// sessionStorage.setItem("age", age);

// console.log(localStorage.getItem("age"));
// console.log(sessionStorage.getItem("age"));

// console.log(localStorage.length);
// console.log(sessionStorage);

// localStorage.removeItem("age");
// console.log(localStorage.key(0));

// TASK 1

const button = document.getElementById("button");
const reminder = document.getElementById("reminder");
const seconds = document.getElementById("seconds");

function creatReminder() {
   this.disabled = true;
   if (reminder.value.trim() === "") {
      alert("Ведите текст напоминания.");
   }
   if (seconds.value < 1) {
      alert("Время задержки должно быть больше одной секунды.");
   }
   const timer = setTimeout(function () {
      alert(reminder.value);
      clearTimeout(timer);
      seconds.value = "";
      reminder.value = "";
      this.disabled = false;
   }, seconds.value * 1000);
}
button.addEventListener("click", creatReminder);

function checkEnter(evt) {
   const that = this;
   if (evt.keyCode === 13) {
      creatReminder.call(that);
   }
}

reminder.addEventListener("keydown", checkEnter);
seconds.addEventListener("keydown", checkEnter);
