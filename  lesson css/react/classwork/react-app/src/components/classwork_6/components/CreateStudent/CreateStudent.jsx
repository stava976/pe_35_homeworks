import React, { useContext, useRef } from 'react'
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { API_SERVER } from '../../config';
import { StudentsContext } from '../StudentsContext/StudentsContext';
import { ThemeContext } from '../ThemeContext/ThemeContext';

const CreateStudent = () => {
  const firstNameRef = useRef();
  const lastNameRef = useRef();
  const {theme} = useContext(ThemeContext);

  const navigate = useNavigate();
  const { onCreate } = useContext(StudentsContext);

  const createNewUser = (e) => {
    e.preventDefault()
    fetch(`${API_SERVER}/student`, {
      method: 'POST',
      headers: {'Content-type' : 'application/json'},
      body: JSON.stringify({
        firstName: firstNameRef.current.value,
        lastName: lastNameRef.current.value
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        onCreate(response);
        navigate("/");
      })
  }



  return (
    <div>
      <h1>Create student</h1>
      <form onSubmit={createNewUser}>
        <label>
          First Name
          <input ref={firstNameRef}/>
        </label>
        <label>
          Last Name
          <input ref={lastNameRef}/>
        </label>
        <button style={theme}>Submit</button>
      </form>
      
    </div>

  );
}

export default CreateStudent