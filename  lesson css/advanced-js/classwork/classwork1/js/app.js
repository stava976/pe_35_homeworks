const mediator = new Mediator();

class App {
  constructor() {
    this.init();
  }

  async init() {
      
      const auth = new Auth();
      mediator.addEventListener('login', this.initDataForLoggedInUser);
  }

  initDataForLoggedInUser = () => {
    const teachersCollection = new TeachersCollection();
    const studentsCollection = new StudentsCollection();  
  }
}
