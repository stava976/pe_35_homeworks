// const gulp = require("gulp");
const { parallel, series } = require("gulp");
const { serv } = require("./gulp-tasks/serv.js");
const { watcher } = require("./gulp-tasks/watch.js");
const { scripts } = require("./gulp-tasks/scripts.js");

exports.default = parallel(serv, watcher, series(scripts));
// exports.dev
