import React, { useState } from 'react';
import { ThemeContext } from '../../components/ThemeContext/ThemeContext';

const THEMES = {
  BLUE: {
    borderColor: '#fff',
    color: '#fff',
    backgroundColor: '#0000ff',
  },
  LIGHT: {
    borderColor: '#0000ff',
    color: '#0000ff',
    backgroundColor: 'transparent',
  },
}

const ThemesContainer = (props) => {
  const [theme, setTheme] = useState('BLUE')
  
  const handleThemeChange = (e) => {
    setTheme(e.target.value);
    console.log(e.target.value);
  }

  const { children } = props;

  return (
    <ThemeContext.Provider value={{
      theme: THEMES[theme],
      themeName: theme,
      onChange: handleThemeChange,
    }}>
      {children}
    </ThemeContext.Provider>
  );
}

export default ThemesContainer;