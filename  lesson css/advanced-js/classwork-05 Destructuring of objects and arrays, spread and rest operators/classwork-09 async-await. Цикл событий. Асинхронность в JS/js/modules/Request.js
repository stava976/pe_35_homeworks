export default class Request {
    constructor(url) {
        this.url = url;
    }
    post(entity, data) {
        return axios({
            method: "post",
            url: this.url + entity,
            data: data,
            headers: {
                "Content-Type": "application/json",
            },
        });
    }
    get(entity) {
        return axios({
            url: this.url + entity,
            headers: {
                "Content-Type": "application/json",
            },
        });
    }
}
