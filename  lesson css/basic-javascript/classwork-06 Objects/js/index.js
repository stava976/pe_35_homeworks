"use strict";

// const user = {
//    name: "Uasya",
//    _age: 18,

//    set age(value) {
//       if (value > 0) {
//          this._age = value;
//       }
//    },
//    get age() {
//       return this._age;
//    },

//    family: {
//       father: {
//          name: "Uasiliy",
//          getName: function () {
//             return this.name;
//          },
//       },
//       mother: "Uasilisa",
//    },
//    getName: function () {
//       return this.name;
//    },
// };

// console.log(user["family"].father.getName.call(user));

// user.age = 30;
// console.log(user.age);
// console.log(user.name);

// console.log(user);
// console.log(user.getName());

// user.maxAge = 100;
// console.log(user);

// const user2 = user;

// console.log(user.name);
// user2.name = "Vitalic";
// user.name = "Uasya";
// console.log(user.name);
// console.log(user2.name);

// const user2 = {};

// for (const key in user) {
//    user2[key] = user[key];
// }

// console.log(user2);
// console.log(user.name);
// user2.name = "Vitalic";

// console.log(user.name);
// console.log(user2.name);

// console.log(user2["getName"]());
// console.log(`${user2["getName"]}`);
// user.count = 1;
// user2.length = 33;
// const user3 = Object(user2);

// console.log(user3);

// TASK 1
// function createUser() {
//    return {
//       name: "Ivan",
//       lastName: "Kutuzov",
//       job: "konstruction",
//       sayHi: function () {
//          console.log(`Привет`);
//       },
//    };
// }
// // createUser().sayHi();
// const user = createUser();
// user.sayHi();

// TASK 2
// function createUser() {
//    return {
//       name: "Ivan",
//       lastName: "Kutuzov",
//       job: "konstruction",
//       sayHi: function () {
//          console.log(`Привет. меня зовут ${this.name} ${this.lastName}`);
//       },
//       changeProperty: function (propertyName, propertyValue) {
//          if (Object.hasOwnProperty.call(this, propertyName)) {
//             this[propertyName] = propertyValue;
//             console.log("Yeah");
//          } else {
//             console.log("Что ты делаешь?");
//          }
//       },
//    };
// }
// createUser().sayHi();
// const user = createUser();
// user.changeProperty("lastName", "job");
// console.log(user);
function createUser() {
   return {
      name: "Ivan",
      lastName: "Kutuzov",
      job: "konstruction",
      sayHi: function () {
         console.log(`Привет. меня зовут ${this.name} ${this.lastName}`);
      },
      changeProperty: function (propertyName, propertyValue) {
         if (Object.hasOwnProperty.call(this, propertyName)) {
            this[propertyName] = propertyValue;
            console.log("Yeah");
         } else {
            console.log("Что ты делаешь?");
         }
      },
      creatProperty: function (propertyName, propertyValue) {
         if (!Object.hasOwnProperty.call(this, propertyName)) {
            this[propertyName] = propertyValue;
            console.log("Yeah");
         } else {
            console.log("Что ты делаешь?");
         }
      },
   };
}
const user = createUser();
user.creatProperty("age", 19);
console.log(user);
