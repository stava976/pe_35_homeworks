// const arr = [1, "value 2", 3, 4, 5];

// const obj = {
//     info: {
//         name: "Uasya",
//     },
//     age: 18,
//     count: 10,
// };

// const [, secondValue, ...rest] = arr;
// console.log(secondValue, rest);

// const {
//     age: userAge = 0,
//     info: { name: userName = "" },
//     count = 0,
// } = obj;
// console.log(userAge, userName, count);

// console.log(...arr);

// function fn(...rest) {
//     console.log(rest);
// }

// fn(1, 2, 3, 4, 5);

// console.log(obj);
// const obj2 = { num: 1, ...obj, num2: 2 };

// obj2.count = 3;
// console.log(obj2);

// TASK1

// class Patient {
//     constructor(height, weight) {
//         this.height = height;
//         this.weight = weight;
//     }
//     getPatientStatus() {
//         let index = this.weight / (((this.height / 100) * this.height) / 100);
//         index = Math.floor(index);
//         if (index >= 10 && index <= 15) {
//             return [index, `анорексия`];
//         }
//         if (index >= 16 && index <= 25) {
//             return [index, `норма`];
//         }
//         if (index >= 26 && index <= 30) {
//             return [index, `лишний вес`];
//         }
//         if (index >= 31 && index <= 35) {
//             return [index, `I степень`];
//         }
//         if (index >= 36 && index <= 40) {
//             return [index, `II степень`];
//         }
//         if (index > 41) {
//             return [index, `III степень`];
//         }
//     }
// }

// const newPatient = new Patient(190, 80);
// console.log(newPatient.getPatientStatus());

// TASK 2

// class User {
//     constructor(name, lastName, ...params) {
//         this["name"] = name;
//         this.lastName = lastName;
//         this.params = {};

//         params.forEach((el) => {
//             const [propName, propValue] = el.split(":");
//             this.params[propName] = propValue;
//         });
//     }
// }

// const user = new User(
//     "Золар",
//     "Аперкаль",
//     "status: глава Юного клана Мескии",
//     "wife: Иврейн"
// );
// console.log(user);

// user = {
//     name: "Золар",
//     lastName: "Аперкаль",
//     params: {
//         status: "глава Юного клана Мескии",
//         wife: "Иврейн",
//     },
// };

// TASK 3
// class Warrior {
//     constructor({ name, status, weapon }) {
//         this.name = name;
//         this.status = status;
//         this.weapon = weapon;
//     }
// }

// const warrior = new Warrior({
//     name: "value",
//     lastName: "value",
//     weapon: "value",
//     status: "value",
//     vid: "value",
// });
// console.log(warrior);
// warrior= {
//    name: "value",
//    weapon: "value",
//    status: "value",
// }

// TASK 4

// const resume = {
//     name: "Илья",
//     lastName: "Куликов",
//     age: 29,
//     city: "Киев",
//     skills: [
//         { name: "Vanilla JS", practice: 5 },
//         { name: "ES6", practice: 3 },
//         {
//             name: "React + Redux",
//             practice: 1,
//         },
//         { name: "HTML4", practice: 6 },
//         { name: "CSS2", practice: 6 },
//     ],
// };

// const vacancy = {
//     company: "SoftServe",
//     location: "Киев",
//     skills: [
//         { name: "Vanilla JS", experience: 3 },
//         { name: "ES6", experience: 2 },
//         { name: "React + Redux", experience: 2 },
//         { name: "HTML4", experience: 2 },
//         { name: "CSS2", experience: 2 },
//         { name: "HTML5", experience: 2 },
//         { name: "CSS3", experience: 2 },
//         { name: "AJAX", experience: 2 },
//         { name: "Webpack", experience: 2 },
//     ],
// };

// class Cv {
//     constructor(resume, vacancy) {
//         this.resume = resume;
//         this.vacancy = vacancy;
//     }

//     checkPercent() {
//         const { skills: resumeSkills } = resume;
//         const { skills: vacancySkills } = vacancy;

//         let perc = 0;

//         vacancySkills.forEach(({ name: vSName, experience: vSExp }) => {
//             let rSkill = resumeSkills.find(function ({ name: rSName }) {
//                 return vSName === rSName;
//             });

//             if (rSkill) {
//                 if (vSExp <= rSkill.practice) {
//                     perc++;
//                 }
//             }
//         });

//         console.log(Math.floor((perc * 100) / vacancySkills.length));
//     }
// }

// const user = new Cv(resume, vacancy);
// user.checkPercent();

// TASK 5

// class MachineNumber {
//     constructor(idMenu, selectorElem, idPlaceHolder) {
//         this.idMenu = idMenu;
//         this.selectorElem = selectorElem;
//         this.idPlaceHolder = idPlaceHolder;
//     }
//     getNumber() {
//         const menu = document.querySelector(`#${this.idMenu}`);
//         let menuNodes = [...menu.querySelectorAll(this.selectorElem)];
//         menu.addEventListener("click", function (event) {
//             console.log(menuNodes.indexOf(event.target) + 1);
//         });
//     }
// }

// const obj = new MachineNumber("menu", ".war-machine", "war-machine-number");
// obj.getNumber();

// TASK 6

class Licence {
    constructor({ age: userAge, wantLicense: wantedLicence }) {
        this.userAge = userAge;
        this.licence = wantedLicence;
    }
    licenceCheck() {
        const driverLicence = [
            { age: 16, type: "A1", time: 2 },
            { age: 16, type: "A2", time: 2 },
            { age: 18, type: "B1", time: 6 },
            { age: 18, type: "B", time: 4 },
            { age: 19, type: "BE", time: 4 },
            { age: 21, type: "D1", time: 3 },
        ];
        const wantedL = driverLicence.find(({ type }) => type === this.licence);
        if (wantedL) {
            return this.userAge >= wantedL.age ? true : false;
        }
        return new Error("Somethig go wrong");
    }
}

const user = {
    age: 17,
    name: "Алексей",
    lastName: "Михайлович",
    wantLicense: "B1",
};

const userLicence = new Licence(user);
console.log(userLicence.licenceCheck());
