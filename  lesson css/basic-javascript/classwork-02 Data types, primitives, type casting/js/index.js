"use strict";

// let name = "Uasya";
// console.log(typeof name);

let someVAriable = 3;
// console.log(typeof someVAriable);
// // console.log(typeof typeof someVAriable);

// someVAriable = undefined;
// console.log(typeof someVAriable);

// someVAriable = true;
// console.log(typeof someVAriable);

// someVAriable = null;
// console.log(typeof someVAriable);

// someVAriable = [1, 2, 3, 4];
// console.log(typeof someVAriable);

// someVAriable = function () {};
// console.log(typeof someVAriable);

// someVAriable = { name: "Uasya", age: 18 };
// console.log(typeof someVAriable);

///////////

// someVAriable = { name: "Uasya", age: 18 } instanceof Object;
// console.log(someVAriable);

//////////////////

// // To Number
// // string to number
// console.log(+"18a");
// console.log(parseInt(" 18a"));
// console.log(parseFloat(" 18a"));
// console.log(Number(""));

// // boolean to number
// console.log(+true);
// console.log(parseInt(true));
// console.log(parseFloat(true));
// console.log(Number(true));

// // null to number
// console.log(+null);
// console.log(parseInt(null));
// console.log(parseFloat(null));
// console.log(Number(null));

// // undefined to number
// console.log(+undefined);
// console.log(parseInt(undefined));
// console.log(parseFloat(undefined));
// console.log(Number(undefined));

// array to number
// console.log(+[undefined, 1, true, null]);
// console.log(parseInt([undefined, 1, true, null]));
// console.log(parseFloat([undefined, 1, true, null]));
// console.log(Number([]));

// // function to number
// console.log(+function fnName() {});
// console.log(parseInt(function fnName() {}));
// console.log(parseFloat(function fnName() {}));
// console.log(Number(function fnName() {}));

// object to number
// console.log(+{});
// console.log(parseInt({ name: "Uasya" }));
// console.log(parseFloat({ name: "Uasya" }));
// console.log(Number({ name: "Uasya" }));

// To string
let age = 33;
// console.log(`${age}`);
// console.log(String(age));
// console.log((33).toString());

// console.log(`${true}`);
// console.log(String(true));

// console.log(`${null}`);
// console.log(String(null));

// console.log(`${undefined}`);
// console.log(String(undefined));

// console.log(`${[1, 2, null, undefined]}`);
// console.log(String([1, 2, null, undefined]));

// console.log(
//    `${function fnName() {
//       console.log(String(undefined));
//    }}`
// );

// console.log(String({ name: "Uasya" }));

// To boolean
// console.log(Boolean(1));
// console.log(!!1);

// console.log(!!"123");
// console.log(!!"");
// console.log(!![1, 2, 3]);
// console.log(!!{ age: 33 });
// console.log(!!null);
// console.log(!!undefined);
// console.log(!!function fnName() {});

// console.log(1 + 1);
// console.log(1 + "1");
// console.log({ name: "Roman" } + "1");
// console.log(1 + +"");
// console.log(+"");
// console.log(1 + null);
// console.log(+null);
// console.log(-null);
// console.log(1 + undefined);
// console.log(1 + NaN);
// console.log(1 + false);
// console.log(+false);
// console.log(1 + true);
// console.log(1 + { name: "Roman" });
// console.log(1 + [33, 22]);
// console.log(1 + []);
// console.log(+[]);

// console.log(1 - 1);
// console.log(1 - "1");
// console.log(1 - "12");
// console.log({ name: "Roman" } - "1");
// console.log(1 - -"");
// console.log(-"");
// console.log(1 - null);
// console.log(-null);
// console.log(+null);
// console.log(1 - undefined);
// console.log(1 - NaN);
// console.log(1 - false);
// console.log(1 - true);
// console.log(1 - { name: "Roman" });
// console.log(1 - [33, 22]);
// console.log(1 - []);

// +"-1";
// +"1";

// console.log(Boolean(0));
// console.log(Boolean(1));
// console.log(Boolean(2));
// console.log(Boolean(3));
// console.log(Boolean(-3));
// console.log(Boolean(null));
// console.log(Boolean(undefined));
// console.log(Boolean(""));
// console.log(Boolean([]));
// console.log(Boolean({}));

/* Second part */
// for (let index = 0; index < 10; index++) {
//    const div = document.createElement("span");
//    div.textContent = index;
//    const color = `rgb(
//         ${Math.floor(Math.random() * 255)},
//         ${Math.floor(Math.random() * 255)},
//         ${Math.floor(Math.random() * 255)})`;

//    div.setAttribute("data-color", color);

//    div.style.color = div.getAttribute("data-color");
//    // div.style.color = color;
//    div.style.fontWeight = "700";
//    div.style.fontSize = "40px";

//    document.body.prepend(div);
// }

// /* Максимальное доступное в языке число с плавающей точкой (дробное). */
// console.log(Number.MAX_VALUE);
// /* Минимальное доступное в языке число с плавающей точкой (дробное). */
// console.log(Number.MIN_VALUE);
// /* Максимальное доступное в языке целое число. */
// console.log(Number.MAX_SAFE_INTEGER);
// /* Минимальное доступное в языке целое число. */
// console.log(Number.MIN_SAFE_INTEGER);
// // /* Значение, описывающее не число (Not a Number). */
// console.log(NaN);
// console.log(typeof NaN);

/**
 * ЗАДАНИЕ 2
 *
 * Объяснить поведение каждой операции.
 */

// const number = 123;
// const string = "hello";

// console.log(number + string);
// console.log(number - string);
// console.log(number * string);

/**
 * ЗАДАНИЕ 3
 * Объяснить поведение каждое операции.
 */
// let number = 1;
// console.log(number);
// console.log(++number);
// console.log(number);
// console.log(number++);
// console.log(number++);
// console.log(number);

// console.log(number);
// console.log(--number);
// console.log(number--);
// console.log(number);

// console.log(!!number--);
// console.log(!!number);

// console.log(0.2 + 0.1);

// let x, y, z;

// x = 6;
// y = 15;
// z = 4;
// console.log((z = --x - y * 5));
// console.log(z);

// x = 6;
// y = 15;
// z = 4;
// console.log((y /= x + (5 % z)));
// y /= ...  тоже самое что y = y / ...

// x = 6;
// y = 15;
// z = 4;
// let s = 6;
// console.log((s += y - x++ * z));
// s = s + y - x++ * z;
// console.log(s);
// s += 2 -> s = s + 2

// isNaN(NaN);

// console.log(isNaN(NaN));
// console.log(isNaN(0));
// console.log(isNaN(+"asv"));

// console.log("b" + "a" + +"a" + "a"); // baNaNa
// console.log(+"a" + "a"); // NaNa

// console.log(number);
// console.log(2 + number);
// console.log(+"null" + "2");

console.log(1 == "1");
console.log(1 === "1");
console.log(true === 1);
console.log(true == 1);
