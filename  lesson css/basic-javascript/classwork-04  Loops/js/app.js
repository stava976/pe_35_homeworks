"use strict";

// const count = 9;
// for (let i = 0; i < count; i++) {
//    console.log(`Hello`);
// }

// for (;;) {
//    console.log("empty loop");
//    break;
// }

// let agree = true,
//    count = 0;

// while (agree) {
//    // some code
//    console.log(count++ + 1);
//    if (count < 5) {
//       //   break;
//       continue;
//    }
//    console.log("23 line");
//    if (count === 10) {
//       agree = false;
//    }
// }

///////////////

// do {
//    // some code
//    console.log(count++ + 1);
//    if (count >= 10) {
//       agree = false;
//    }
// } while (agree);

// let b;
// a = 444;
// console.log(b);
// console.log(window.a);
// console.log(window);

// TASK 1

// for (let number = 0; number <= 300; number++) {
//    if (number % 2 !== 0 && number % 5 !== 0) {
//       console.log(number);
//    }
// }

// TASK 2

// let a;
// let b;

// do {
//    a = prompt("Number1");
// } while (!a || a.trim() === "" || isNaN(a));

// do {
//    b = prompt("Number2");
// } while (!b || b.trim() === "" || isNaN(b));

//  console.log('«Поздравляем. Введённые вами числа: «'+a+'» и «'+b+'»');
// console.log(`«Поздравляем. Введённые вами числа: «${a}» и «${b}»`);

// TASK 3
// let name;
// let lastName;
// let year;

// do {
//    name = prompt("Введи имя!!!");
//    lastName = prompt("Введи фамилию!!!");
//    year = prompt("Год рождения!!!-.-");
// } while (
//    !name ||
//    name === "" ||
//    !lastName ||
//    lastName === "" ||
//    !year ||
//    year === "" ||
//    isNaN(+year) ||
//    year < 1910 ||
//    year > 2021
// );
// console.log(
//    "Добро пожаловать, родившийся в " + year + " " + name + " " + lastName
// );

// TASK 4
// let name;
// let score;
// do {
//    name = prompt("Your name, Lastname");
//    score = prompt("Your score");
// } while (
//    !name ||
//    name === "" ||
//    !score ||
//    isNaN(+score) ||
//    score < 0 ||
//    score > 100
// );

// let point;

// if (score >= 95 && score <= 100){
//     point = "A";
// }
