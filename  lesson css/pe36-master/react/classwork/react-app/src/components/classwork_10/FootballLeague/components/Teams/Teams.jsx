import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchTeams, addTeam, teamEditStarted, editTeam, deleteTeam } from '../../redux/teams/teams.actions';
import { teamsSelector } from '../../redux/teams/teams.selectors';
import './Teams.css';

const Teams = () => {
  const teams = useSelector(teamsSelector);
  const teamToEditId = useSelector((state) => state.teams.teamToEditId);
  const addRequestState = useSelector((state) => state.teams.addRequestState);
  const fetchRequestState = useSelector((state) => state.teams.fetchRequestState);
  const editRequestState = useSelector((state) => state.teams.editRequestState);
  const coaches = useSelector(state => state.coaches.coachesList)
 
  const formRef = useRef();
  const dispatch = useDispatch();
  const [teamToEdit, setTeamToEdit] = useState(null);

  useEffect(()=> {
    dispatch(fetchTeams());
  },[])

  const onSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData(formRef.current);
    let newTeamData = {};

    for (const pair of formData.entries()) {
      newTeamData = {
        ...newTeamData,
        [pair[0]]: pair[1], 
      };
    }

    dispatch(addTeam(newTeamData));
  }

  useEffect(() => {
    const team = teams.find((team) => {
      return team.id === teamToEditId
    })
    setTeamToEdit(team);
  }, [teamToEditId, teams])

  const handleEditRequest = (e) => {
    const id = +e.target.dataset.id;

    dispatch(teamEditStarted(id))
  }

  const handleSaveRequest = () => {
    dispatch(editTeam(teamToEdit))
  }

  const handleDeleteRequest = (e) => {
    const id = +e.target.dataset.id;

    dispatch(deleteTeam(id));
  }

  const handleNameChange = (e) => {
    const name = e.target.value;
    const newTeamToEdit = {...teamToEdit, name};
    setTeamToEdit(newTeamToEdit);
  }

  const handleCityChange = (e) => {
    const city = e.target.value;
    const newTeamToEdit = {...teamToEdit, city};
    setTeamToEdit(newTeamToEdit);
  }

  const handleCoachChange = (e) => {
    const coachId = e.target.value;
    const newTeamToEdit = {...teamToEdit, coachId: +coachId};
    setTeamToEdit(newTeamToEdit);
  }

  return (
    <div>
      { editRequestState === 'loading' &&
        <div className="loader">
          <h3>Loading...</h3>
        </div>
      }
      { fetchRequestState === 'loading' &&
        <div className="loader">
          <h3>Loading...</h3>
        </div>
      }
      { addRequestState === 'loading' &&
        <div className="loader">
          <h3>Loading...</h3>
        </div>
      }
      <h1>Teams</h1>
      {
        teams.map((team) => {
          return (<div key={team.id} style={{ margin: '18px 12px 12px' }}>
            { 
              teamToEdit?.id === team.id ? 
              <>
                Team name: <h3 style={{ margin: '0 0 10px' }}>
                    <input onChange={handleNameChange} value={teamToEdit.name} />
                  </h3>
                from: <input onChange={handleCityChange} value={teamToEdit.city} /> <br/>
                coach: <select value={teamToEdit.coachId} onChange={handleCoachChange}>{
                  coaches.map(coach => {
                  return <option key={coach.id} value={coach.id}>{coach.name}</option>
                  })
                }</select>
                <button data-id={team.id} onClick={handleSaveRequest}>Save</button>
              </> :
              <>
                Team name: <h3 style={{ margin: '0 0 10px' }}>{team.name}</h3>
                from: {team.city} <br/>
                coach: {team.coach?.name} <br />
                <button data-id={team.id} onClick={handleEditRequest}>Edit</button>
                <button data-id={team.id} onClick={handleDeleteRequest}>Delete</button>
              </>
            }

          </div>);
        })
      }
      <br/>
      <form ref={formRef} onSubmit={onSubmit}>
        <label>
          Name:
          <input name="name" />
        </label>
        <br/>
        <label>
          City:
          <input name="city" />
        </label>
        <br/>
        <label>
          Coach:
          <input name="coach" />
        </label>
        <br/>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default Teams;
