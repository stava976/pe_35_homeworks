class Mediator {
  constructor() {
    this.callbacks = {
    };   
  }
  addEventListener(eventName, callback) {
    const hasEvent = Array.isArray(this.callbacks[eventName]);

    if (!hasEvent) {
      this.callbacks[eventName] = [];
    }
    this.callbacks[eventName].push(callback); 
    console.log(this.callbacks);
  }
  removeEventListener(eventName, listenerToRemove) {
    this.callbacks[eventName] = 
      this.callbacks[eventName].filter((listener) => {
        return listenerToRemove !== listener;
      });
  }
  publishEvent(eventName) {
    this.callbacks[eventName].forEach((callback) => {
      callback();
    });
  }
}