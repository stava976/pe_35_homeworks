import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { DECREASE, INCREASE, RESET } from './redux/counter.actions';

const Counter = () => {
  const counter = useSelector((state) => state.counter);
  const dispatch = useDispatch();

  const onClickPlus = () => {
    dispatch({ type: INCREASE });
  }

  const onClickMinus = () => {
    dispatch({ type: DECREASE });
  }

  const onReset = () => {
    dispatch({ type: RESET });
  }

  return (
    <div>
      <h1>{counter}</h1>
      <button onClick={onClickPlus}>+</button>
      <button onClick={onClickMinus}>-</button>
      <button onClick={onReset}>reset</button>
    </div>
  )
}

export default Counter;
