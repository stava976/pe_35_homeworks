import React from 'react';
import './FormWithValidation.css';
import Input from './Input';

const emailRegexp = /(.+)@(.+){2,}\.(.+){2,}/;
const validateEmail = (email) => {
  return emailRegexp.test(email);
}

class FormWithValidation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isEmailInvalid: false,
      isPasswordInvalid: false,
    };
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value,
      isEmailInvalid: !validateEmail(e.target.value),
    });
  }

  onPasswordChange = (e) => {
    this.setState({
      password: e.target.value,
      isPasswordInvalid: e.target.value === '',
    });
  }

  render() {
    return (
      <div>
        <h1>Login</h1>
        <form>
          <Input 
            value={this.state.email}
            onChange={this.onEmailChange}
            label="Email"
            error={
              this.state.isEmailInvalid 
                ? 'Please, enter valid email'
                : null
              }
          />

          <Input 
            value={this.state.password}
            onChange={this.onPasswordChange}
            label="Password"
            error={
              this.state.isPasswordInvalid 
                ? 'Please, enter password'
                : null
              }
          />

          <button>Submit</button>
        </form>
      </div>
    );
  }
}

export default FormWithValidation;
