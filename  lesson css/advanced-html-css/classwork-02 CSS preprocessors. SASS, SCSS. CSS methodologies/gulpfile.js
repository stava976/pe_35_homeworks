// const gulp = require("gulp");
const { parallel, series } = require("gulp");
const { serv } = require("./gulp-tasks/serv.js");
const { watcher } = require("./gulp-tasks/watch.js");
const { scripts } = require("./gulp-tasks/scripts.js");
const { styles } = require("./gulp-tasks/styles.js");

exports.default = parallel(serv, watcher, series(styles, scripts));
// exports.dev
// exports.styles = styles;
