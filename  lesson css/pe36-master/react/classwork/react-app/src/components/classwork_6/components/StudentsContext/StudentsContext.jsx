import React from "react";

export const StudentsContext = React.createContext({
  students: null,
  onCreate: () => {},
  onDelete: () => {},
  onCollectionFirstRender: () => {},
  onEdit: () => {},
});
