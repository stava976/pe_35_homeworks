export const teamsSelector = (state) => {
  return state.teams.teamsList.map(team => {
    return {
      ...team,
      coach: state.coaches.coachesList.find(coach => team.coachId === coach.id)
    }
  })
}