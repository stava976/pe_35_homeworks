"use strict";

// const age = 17;

// if (){}
// if (age > 16 && true) {
//    // to do
//    console.log("true done");
//    if (true || false) {
//       console.log("true inside");
//    } else {
//       // to do
//    }
// } else if (age <= 16) {
//    // to do
//    console.log("Error");
// } else {
//    // to do
//    console.log("something wrong");
// }
// console.log(age > 16 || false);

// console.log("1" == true);
// console.log("1" === true);

// const somvar = 5;

// if (!somvar) {
//    console.log("working");
// } else {
//    console.log("not working");
// }

// const number = 4;
// let result;

// if (number % 2 === 0) {
//    result = number * 2;
// } else {
//    result = number - 1;
// }

// (condition) ? if condition -> true : if condition -> false;
// result = number % 2 === 0 ? number * 2 : number - 1;
// console.log(result);

// const answer = "w";

// switch (answer) {
//    case "a":
//       // to do
//       console.log("Your answer is a");
//       break;

//    case "b":
//       // to do
//       console.log("Your answer is b");
//       break;

//    case "c":
//       // to do
//       console.log("Your answer is c");
//       break;

//    case "d":
//       // to do
//       console.log("Your answer is d");
//       break;

//    default:
//       console.log("Wrong answer");
//       break;
// }

// TASK 1
// let number = prompt("Вееди число!!");

// if (number === "" || isNaN(number) || !number) {
//    alert("Необходимо ввести число!");

//    number = prompt("Вееди число!!");
//    if (number === "" || isNaN(number) || !number) {
//       alert("Необходимо ввести число!");
//    } else {
//       if (number % 2 === 0) {
//          console.log("Ваше число чётное.");
//       } else {
//          console.log("Ваше число не чётное.");
//       }
//    }
// } else {
//    if (number % 2 === 0) {
//       console.log("Ваше число чётное.");
//    } else {
//       console.log("Ваше число не чётное.");
//    }
// }

// TASK 2

// let name = prompt("Your Name");

// switch (name) {
//    case "Mike":
//       console.log("Hello Mike - CEO");
//       break;
//    case "Jane":
//       console.log("Hello Jane — CTO;");
//       break;
//    case "Walter":
//       console.log("Hello Walter — программист");
//       break;
//    case "Oliver":
//       console.log("Oliver — менеджер");
//       break;
//    case "John":
//       console.log("John — уборщик");
//       break;
//    default:
//       console.log("«Пользователь не найден.»");
// }

// if (name === "Mike") {
//    console.log("Hello Mike - CEO");
// } else if (name === "Jane") {
//    console.log("Hello Jane — CTO;");
// } else if (name === "Walter") {
//    console.log("Hello Walter — программист");
// } else if (name === "Oliver") {
//    console.log("Oliver — менеджер");
// } else if (name === "John") {
//    console.log("John — уборщик");
// } else {
//    console.log("«Пользователь не найден.»");
// }

// TASK 3
let x = prompt("Fill a number");
let y = prompt("Fill a number");
let z = prompt("Fill a number");

// x === null -> !x
// x === false -> !x
// x === undefined -> !x
// x === "" -> !x

if ((isNaN(x) || !x) && (isNaN(y) || !y) && (isNaN(z) || !z)) {
   alert("Ошибка! Одно из введённых чисел не является числом.");
} else {
   if (x > y && x > z) {
      console.log(x);
   } else if (y > x && y > z) {
      console.log(y);
   } else {
      console.log(z);
   }
}
x = 5;
if (!x) {
}
