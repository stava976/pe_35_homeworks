// const list = document.getElementById("list");
// const items = document.getElementsByClassName("list-item");
// const body = document.body;

// const clickHandler = function (evt) {
//    console.log("click on ");
//    console.log(this);
//    console.log(evt.target);
// };

// Array.from(items).forEach((el) => {
//    el.addEventListener("click", clickHandler, true);
// });
// body.addEventListener("click", clickHandler, false);

// list.addEventListener("click", clickHandler, true);

// list.addEventListener(
//    "click",
//    (evt) => {
//       console.log(`click on list`);
//       console.log(evt);
//       evt.stopPropagation();
//       evt.target.style.fontWeight = 700;
//    },
//    true
// );

// Array.from(items).forEach((el) => {
//    el.addEventListener(
//       "click",
//       (evt) => {
//          console.log(`click on item`);
//       },
//       true
//    );
// });

// TASK 1
// const ul = document.querySelector("#menu");

// ul.addEventListener("click", (evt) => {
//    evt.preventDefault();

//    const liItems = document.querySelectorAll("#menu li");
//    Array.from(liItems).forEach((element) => {
//       element.classList.remove("active");
//    });
//    evt.target.parentElement.classList.add("active");
// });

// TASK 2

const list = document.getElementById("messages");

list.addEventListener("click", function (evt) {
   if (evt.target.closest(".close-btn")) {
      evt.target.closest(".pane").remove();
   }
});
