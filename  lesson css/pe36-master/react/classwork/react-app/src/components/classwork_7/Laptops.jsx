import React from 'react';

const LAPTOPS = [
  {
    brand: 'Mac',
    size: '13',
    color: 'grey',
    CPU: 2.2,
  },
  {
    brand: 'HP',
    size: '16',
    color: 'green',
    CPU: 2.4,
  },
  {
    brand: 'Asus',
    size: '12',
    color: 'white',
    CPU: 1.8,
  },
  {
    brand: 'Dell',
    size: '17',
    color: 'black',
    CPU: 3.0,
  },
  {
    brand: 'XXXX',
    size: '17',
    color: 'black',
    CPU: 3.0,
  }
]

const sum = [2, 4, 6, 7, 8].reduce((initValue, elem) => {
  if (elem % 2 === 0) {
    return initValue + elem;
  } 
  return initValue;
}, 0);

const Laptops = () => {
  const totalData = LAPTOPS.reduce((acc, laptop) => {
    const newTotal = acc.total + 1;
    const newSmallTotal = acc.small + (Number(laptop.size) < 15 ? 1 : 0);

    return {
      total: newTotal,
      small: newSmallTotal,
    };
  }, {
    total: 0,
    small: 0,
  });

  return (
    <div>
      <div>Total: {totalData.total}</div>
      <div>Small: {totalData.small}</div>
    </div>
  );
}

export default Laptops;
