import React from 'react';
import PropTypes from 'prop-types';

class NumberForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <input onChange={this.props.onChange} value={this.props.value} />
        <button onClick={this.props.onSubmit}>Submit</button>
      </>
    )
  }
}

NumberForm.propTypes = {
  value: PropTypes.string.isRequired,
}

export default NumberForm;