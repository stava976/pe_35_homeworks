"use strict";
const ipData = document.querySelector(".ip-data");

async function getIpData() {
    const UIP = await fetch(`http://api.ipify.org/?format=json`);
    const user = await UIP.json();
    const responseAdressIp = await fetch(`http://ip-api.com/json/${user.ip}?fields=status,message,continent,country,region,city`);
    const adressIp = await responseAdressIp.json();
    ipData.insertAdjacentHTML("afterbegin", `
    <p>Континент: ${adressIp.continent}</p>
    <p>Cтрана: ${adressIp.country}</p>
    <p>Регион: ${adressIp.region}</p>
    <p>Город: ${adressIp.city}</p>      
  `);
    console.log(adressIp);
}


